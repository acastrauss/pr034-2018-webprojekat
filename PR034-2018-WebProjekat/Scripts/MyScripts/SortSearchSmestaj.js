﻿
var smestajeviToShow = [];

// key: smestaj.MyGuid, value: broj slobodnih smestajnih jedinica
var slobodneSJzaSmestaj = {}

$(document).ready(function () {

    console.log('Sts:' + JSON.stringify(smestajeviToShow));

    $('#sortBtn').click(function () {
        $('#allAccomTable').html('');
        smestajeviToShow = [...sviSmestaji];

        let byWhat = new Number($('#sortByWhat').val());
        let how = new Number($('#sortHow').val());

        if (byWhat == 0) {
            // po nazivu
            smestajeviToShow = smestajeviToShow.sort((a, b) => {
                if (how == 0) {
                    if (a.Naziv < b.Naziv) return -1;
                    if (a.Naziv > b.Naziv) return 1;
                    return 0;
                }
                else {
                    if (a.Naziv < b.Naziv) return 1;
                    if (a.Naziv > b.Naziv) return -1;
                    return 0;
                }
            });

            showSmestajManagement(smestajeviToShow);
        }
        else if (byWhat == 1) {
            // po datumu pocetka
            smestajeviToShow = smestajeviToShow.sort((a, b) => {
                if (how == 0) {
                    if (a.SmestajneJedinice.length < b.SmestajneJedinice.length) return -1;
                    if (a.SmestajneJedinice.length > b.SmestajneJedinice.length) return 1;
                    return 0;
                }
                else {
                    if (a.SmestajneJedinice.length < b.SmestajneJedinice.length) return 1;
                    if (a.SmestajneJedinice.length > b.SmestajneJedinice.length) return -1;
                    return 0;
                }
            });

            showSmestajManagement(smestajeviToShow);
        }
        else {

            let smestajGuids = [];

            sviSmestaji.forEach(s => smestajGuids.push(s.MyGuid));

            (new Promise((resolve, reject) => {
                $.ajax({
                    url: 'api/Smestaj/GetFreeSJForMultipleSmestajevi',
                    type: 'GET',
                    data: $.param({ smestajGuids: smestajGuids }, true),
                    success: function (result) {
                        resolve(result);
                    },
                    error: function (error) {
                        alert(JSON.stringify(error));
                    }
                })
            }))
                .then((data) => {
                    slobodneSJzaSmestaj = JSON.parse(data);
                    // po datumu zavrsetka
                    smestajeviToShow = smestajeviToShow.sort((a, b) => {
                        if (how == 0) {
                            if (slobodneSJzaSmestaj[a.MyGuid] < slobodneSJzaSmestaj[b.MyGuid]) return -1;
                            if (slobodneSJzaSmestaj[a.MyGuid] > slobodneSJzaSmestaj[b.MyGuid]) return 1;
                            return 0;
                        }
                        else {
                            if (slobodneSJzaSmestaj[a.MyGuid] < slobodneSJzaSmestaj[b.MyGuid]) return 1;
                            if (slobodneSJzaSmestaj[a.MyGuid] > slobodneSJzaSmestaj[b.MyGuid]) return -1;
                            return 0;
                        }
                    });

                    showSmestajManagement(smestajeviToShow);
                });


        }

    });

    $('#cancelSearch').click(function () {
        $('#allAccomTable').html('');

        $('#tipSmestajaSelect').val('');
        $('#nazivSmestaja').val('');
        $('#bazen').prop('checked', false);
        $('#spaCentar').prop('checked', false);
        $('#invaliditet').prop('checked', false);
        $('#wifi').prop('checked', false);

        smestajeviToShow = [...sviSmestaji];

        showSmestajManagement(smestajeviToShow);
    });

    $('#doSearch').click(function () {
        smestajeviToShow = [...sviSmestaji];

        $('#allAccomTable').html('');

        let tipSmestaja = new Number($('#tipSmestajaSelect').val()) - 1;
        let naziv = $('#nazivSmestaja').val();
        let bazen = $('#bazen').is(':checked');
        let spa = $('#spaCentar').is(':checked');
        let inv = $('#invaliditet').is(':checked');
        let wifi = $('#wifi').is(':checked');

        
        let indxToRemove = [];

        if (tipSmestaja != -1) {
            for (let i = 0; i < smestajeviToShow.length; i++) {
                if (smestajeviToShow[i].TipSmestaja != tipSmestaja)
                    indxToRemove.push(i);
            }
        }
        if (naziv) {
            for (let i = 0; i < smestajeviToShow.length; i++) {
                if (!smestajeviToShow[i].Naziv.toLowerCase().includes(naziv))
                    indxToRemove.push(i);
            }
        }
        if (bazen) {
            for (let i = 0; i < smestajeviToShow.length; i++) {
                if (!smestajeviToShow[i].Bazen)
                    indxToRemove.push(i);
            }
        }
        if (spa) {
            for (let i = 0; i < smestajeviToShow.length; i++) {
                if (!smestajeviToShow[i].SpaCentar)
                    indxToRemove.push(i);
            }
        }
        if (inv) {
            for (let i = 0; i < smestajeviToShow.length; i++) {
                
                if (!smestajeviToShow[i].Invaliditet) {
                    indxToRemove.push(i);
                    console.log(smestajeviToShow[i].Invaliditet);
                    console.log(smestajeviToShow[i].Naziv);
                }
            }
        }
        if (wifi) {
            for (let i = 0; i < smestajeviToShow.length; i++) {
                if (!smestajeviToShow[i].Wifi)
                    indxToRemove.push(i);
            }
        }

        indxToRemove = [...new Set(indxToRemove)];

        for (let i = 0; i < indxToRemove.length; i++) {
            
            smestajeviToShow.splice(indxToRemove[i], 1);
            for (let j = i + 1; j < indxToRemove.length; j++) {
                indxToRemove[j]--;
            }
        }

        showSmestajManagement(smestajeviToShow);
    });

});

