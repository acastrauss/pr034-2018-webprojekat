﻿
var oldArrangements = [];

$(document).ready(function () {

    $('#searchSortAranzmani').load('SortSearchAranzmani.html');

    $.ajax({
        url: 'api/Aranzmani/GetAll',
        type: 'GET',
        success: function (result) {
            
            let availableAr = false;

            result[0].forEach((arIndx) => {
                let ar = JSON.parse(arIndx);
                if (new Date(String(ar.VremeNalazenja)) > new Date()) return;

                oldArrangements.push(ar);

                if (!availableAr) {
                    let p = document.createElement('p');
                    p.innerHTML = 'Past arrangements:';
                    $('#pastArTitle').append(p);
                    availableAr = true;
                }
            });

            oldArrangements.sort((a, b) => new Date(String(b.DatumZavrsetka)) - new Date(String(a.DatumZavrsetka)))
                .forEach((arObj => { showOneAr(arObj); }));

            allArrangmenets = [...oldArrangements];
            toShowArrangements = [...oldArrangements];
        },
        error: function (result) {}
    });
});

function showOneAr(arrangement)
{
    let tbl = document.createElement('table');
    let tr = document.createElement('tr');
    let td = document.createElement('td');
    td.append(tbl);
    tr.append(td);
    $('#aranzmaniTable').append(tr);

    tbl.border = '1px solid black';
    aranzmanBasicTable(arrangement, tbl);
    aranzmanMoreInfoTable(arrangement, tbl);
}