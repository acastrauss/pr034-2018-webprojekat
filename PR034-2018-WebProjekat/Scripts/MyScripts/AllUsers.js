﻿$(document).ready(function () {

    $('#datumRodjenja').attr('max', new Date().toISOString().split('T')[0]);

    $.ajax({
        url: 'api/Users/GetAllUsers',
        type: 'GET',
        success: function (results) {
            results.forEach(user => {

                let tr = document.createElement('tr');
                let td1 = document.createElement('td');
                let td2 = document.createElement('td');
                let td3 = document.createElement('td');
                let td4 = document.createElement('td');
                let td5 = document.createElement('td');
                let td6 = document.createElement('td');
                let td7 = document.createElement('td');

                $('#usersTable').append(tr);
                tr.append(td1);
                tr.append(td2);
                tr.append(td3);
                tr.append(td4);
                tr.append(td5);
                tr.append(td6);
                tr.append(td7);

                td1.innerHTML = user.KorisnickoIme;
                td2.innerHTML = user.Ime;
                td3.innerHTML = user.Prezime;
                td4.innerHTML = user.Pol;
                td5.innerHTML = user.Email;
                td6.innerHTML = new Date(new String(user.DatumRodjenja));
                td7.innerHTML = uloga_korisnika[new Number(user.UlogaKorisnika)];
            });
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });

    $('#registerManagerBtn').click(function () {
        if ($('registerManagerDiv:hidden'))
            $('#registerManagerDiv').show();
        else
            $('#registerManagerDiv').hide();
    });

    $('#saveNewManager').click(function () {
        if (!validateManager()) return;

        var jsonManager = {
            KorisnickoIme: $('#username').val(),
            Lozinka: $('#password').val(),
            Ime: $('#ime').val(),
            Prezime: $('#prezime').val(),
            Email: $('#email').val(),
            UlogaKorisnika: 1,
            DatumRodjenja: new Date(new String($('#datumRodjenja').val()))
        }

        $.ajax({
            url: 'api/Users',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(jsonManager),
            success: function (result) {
                alert('Menadzer registrovan');
                location.reload();
            },
            error: function (result) {
                alert(json['Message']);
            }
        });
    })
});

function validateManager() {
    if ($('#username').val().length < 1) {
        $('#username').css('border', '1px solid red');
        return false;
    }
    else {
        $('#username').css('border', '');
    }

    if ($('#password').val().length < 1) {
        $('#password').css('border', '1px solid red');
        return false;
    }
    else {
        $('#password').css('border', '');
    }

    if ($('#ime').val().length < 1) {
        $('#ime').css('border', '1px solid red');
        return false;
    }
    else {
        $('#ime').css('border', '');
    }

    if ($('#prezime').val().length < 1) {
        $('#prezime').css('border', '1px solid red');
        return false;
    }
    else {
        $('#prezime').css('border', '');
    }

    var re = /^[^\s@]+@[^\s@]+$/;

    if (!re.test($('#email').val())) {
        $('#email').css('border', '1px solid red');
        return false;
    }
    else {
        $('#email').css('border', '');
    }

    if ($('#datumRodjenja').val() == '') {
        $('#datumRodjenja').css('border', '1px solid red');
        return false;
    }
    else {
        $('#datumRodjenja').css('border', '');
    }
    return true;

}