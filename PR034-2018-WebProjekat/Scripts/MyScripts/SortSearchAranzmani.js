﻿$(document).ready(function () {

    $('#doSearch').click(function () {

        $('#aranzmaniTable').html('');
        toShowArrangements = [...allArrangmenets];

        let startDateMin = new Date($('#searchStartDateMin').val());
        let startDateMax = new Date($('#searchStartDateMax').val())
        let endDateMin = new Date($('#searchEndDateMin').val())
        let endDateMax = new Date($('#searchEndDateMax').val())

        let tipPrevoza = new Number($('#searchTipPrevoza').val()) - 1;
        let tipAranzmana = new Number($('#searchTipAr').val()) - 1;

        let nazivAr = $('#searchNaziv').val();

        let indxsToRemove = [];

        if (startDateMin) {
            for (let i = 0; i < toShowArrangements.length; i++) {
                let ar = toShowArrangements[i];

                if (new Date(String(ar.DatumPocetka)) < startDateMin)
                    indxsToRemove.push(i);
            }
        }

        if (startDateMax) {
            for (let i = 0; i < toShowArrangements.length; i++) {
                let ar = toShowArrangements[i];

                if (new Date(String(ar.DatumPocetka)) > startDateMax) {
                    indxsToRemove.push(i);
                }
            }
        }

        if (endDateMin) {
            for (let i = 0; i < toShowArrangements.length; i++) {
                let ar = toShowArrangements[i];

                if (new Date(String(ar.DatumZavrsetka)) < endDateMin)
                    indxsToRemove.push(i);
            }
        }

        if (endDateMax) {
            for (let i = 0; i < toShowArrangements.length; i++) {
                let ar = toShowArrangements[i];

                if (new Date(String(ar.DatumZavrsetka)) > endDateMax)
                    indxsToRemove.push(i);
            }
        }

        if (tipPrevoza != -1) {
            for (let i = 0; i < toShowArrangements.length; i++) {
                let ar = toShowArrangements[i];

                if (ar.TipPrevoza != tipPrevoza)
                    indxsToRemove.push(i);
            }
        }

        if (tipAranzmana != -1) {
            for (let i = 0; i < toShowArrangements.length; i++) {
                let ar = toShowArrangements[i];

                if (ar.TipAranzmana != tipAranzmana)
                    indxsToRemove.push(i);
            }
        }

        if (nazivAr) {

            for (let i = 0; i < toShowArrangements.length; i++) {
                let ar = toShowArrangements[i];
                console.log(i);
                if (!ar.Naziv.toLowerCase().includes(nazivAr.toLowerCase()))
                    indxsToRemove.push(i);
            }
        }

        indxsToRemove = [...new Set(indxsToRemove)];

        for (let i = 0; i < indxsToRemove.length; i++) {
            toShowArrangements.splice(indxsToRemove[i], 1);
            for (let j = i + 1; j < indxsToRemove.length; j++) {
                indxsToRemove[j]--;
            }
        }

        setToShowArsInTable();
    });

    $('#cancelSearch').click(function () {
        $('#searchStartDateMin').val('');
        $('#searchStartDateMax').val('');
        $('#searchEndDateMin').val('');
        $('#searchEndDateMax').val('');
        $('#searchTipPrevoza').val(0);
        $('#searchTipAr').val(0);
        $('#searchNaziv').val('');


        $('#aranzmaniTable').html('');

        toShowArrangements = [...allArrangmenets];

        if (location.href.includes('OldArrangements.html')) {
            toShowArrangements.sort((a, b) => new Date(String(b.DatumPocetka)) - new Date(String(a.DatumPocetka)));
        }
        else {
            toShowArrangements.sort((a, b) => new Date(String(a.DatumPocetka)) - new Date(String(b.DatumPocetka)));
        }

        setToShowArsInTable();
    });

    $('#sortBtn').click(function () {

        $('#aranzmaniTable').html('');

        let byWhat = new Number($('#sortByWhat').val());
        let how = new Number($('#sortHow').val());

        if (byWhat == 0) {
            // po nazivu
            toShowArrangements = toShowArrangements.sort((a, b) => {
                if (how == 0) {
                    if (a.Naziv < b.Naziv) return -1;
                    if (a.Naziv > b.Naziv) return 1;
                    return 0;
                }
                else {
                    if (a.Naziv < b.Naziv) return 1;
                    if (a.Naziv > b.Naziv) return -1;
                    return 0;
                }
            });
        }
        else if (byWhat == 1) {
            // po datumu pocetka
            toShowArrangements = toShowArrangements.sort((a, b) => {
                if (how == 0) {
                    if (a.DatumPocetka < b.DatumPocetka) return -1;
                    if (a.DatumPocetka > b.DatumPocetka) return 1;
                    return 0;
                }
                else {
                    if (a.DatumPocetka < b.DatumPocetka) return 1;
                    if (a.DatumPocetka > b.DatumPocetka) return -1;
                    return 0;
                }
            });
        }
        else {
            // po datumu zavrsetka
            toShowArrangements = toShowArrangements.sort((a, b) => {
                if (how == 0) {
                    if (a.DatumZavrsetka < b.DatumZavrsetka) return -1;
                    if (a.DatumZavrsetka > b.DatumZavrsetka) return 1;
                    return 0;
                }
                else {
                    if (a.DatumZavrsetka < b.DatumZavrsetka) return 1;
                    if (a.DatumZavrsetka > b.DatumZavrsetka) return -1;
                    return 0;
                }
            });
        }

        setToShowArsInTable();
    });

});

function setToShowArsInTable()
{
    toShowArrangements.forEach(x => {

        let tr = document.createElement('tr');
        let td = document.createElement('td');
        let tblAr = document.createElement('table');
        $('#aranzmaniTable').append(tr);
        tr.append(td);
        td.append(tblAr);

        aranzmanBasicTable(x, tblAr);
        aranzmanMoreInfoTable(x, tblAr, true);
    });
}