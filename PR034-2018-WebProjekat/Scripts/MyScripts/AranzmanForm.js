﻿
var izabraniSmestaj;
var smestajObj;
var currentSmestajevi = [];
var imgPath = '';

var guidForSmestaj = '';

$(document).ready(function () {

    fillAr();

    $('#smestajRow').hover(function () {
        if ($('#smestajNaziv').html().length == 0) return;
        $('#smestajDetails').css('display', 'block');
        $('#smestajDetails').css('z-index', '9');
    },
    function () {
        if ($('#smestajNaziv').html().length == 0) return;
        $('#smestajDetails').css('display', 'none');
    }
    );

    let today = new Date().toISOString().split('T')[0];
    $('#datumPocetka').attr('min', today);
    $('#datumZavrsetka').attr('min', today);
    $('#vremeNalazenjaDatum').attr('min', today);

    $(document).on('click', '#dodajSmestaj', function () {

        if ($('#noviSmestaj').length > 0 && $('#postojeciSmestaj').length > 0)
            return;

        let noviSmestaj = document.createElement('button');
        noviSmestaj.id = 'noviSmestaj';
        noviSmestaj.innerText = 'Dodaj novi smestaj';

        let postojeciSmestaj = document.createElement('button');
        postojeciSmestaj.id = 'postojeciSmestaj';
        postojeciSmestaj.innerText = 'Dodaj vec postojeci';

        $('#floating-panel').append(noviSmestaj);
        $('#floating-panel').append(postojeciSmestaj);
    });
    
    $(document).on('click', '#noviSmestaj', function () {
        $('#floating-panel').children().each(function () {
            this.remove();
        });

        $('#dodajSmestaj').click();

        let div = document.createElement('div');
        div.id = 'smestajFormDiv';
        $(div).load('SmestajForm.html');
        $('#floating-panel').append(div);
    });

    $(document).on('click', '#postojeciSmestaj', function () {
        if ($('#allSmestaj').length > 0)
            return;

        $('#floating-panel').children().each(function () {
            this.remove();
        });

        $('#dodajSmestaj').click();

        $.ajax({
            url: 'api/Smestaj',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            success: function (result) {
                currentSmestajevi = [];

                for (let i = 0; i < result.length; i++) {
                    var jsonSmestaj = JSON.parse(result[i]);
                    currentSmestajevi.push(jsonSmestaj);
                }
                smestajTableShow(currentSmestajevi);
            },  
            error: function (result) {
                var json = JSON.parse(result.responseText);
                alert(json['Message']);
            },
        });
    });

    $(document).on('click', '#dodajAranzman', function () {

        if (!validateAranzman()) return;

        let adrArr = $('#adressText').val().split(',')
        
        let vnDate = new Date($('#vremeNalazenjaDatum').val());
        let vnv = $('#vremeNalazenjaVreme').val();
        vnDate.setHours(vnv.split(':')[0]);
        vnDate.setMinutes(vnv.split(':')[1]);
        
        let arrAdresa = adrArr[0].split(' ');

        let ulica = '';
        let broj = 0;

        for (let i = 0; i < arrAdresa.length; i++)
        {    
            if (i == arrAdresa.length - 1 && !isNaN(arrAdresa[i]))
            {
                broj = Number(arrAdresa[i]);
            }
            else
            {
                ulica += arrAdresa[i] + ' ';
            }
        }

        ulica = ulica.trim();

        jsonAranzman = {
            Naziv: $('#naziv').val(),
            TipAranzmana: Number($('#tipAranzmana').val()),
            TipPrevoza: Number($('#tipPrevoza').val()),
            LokacijaPutovanja: $('#lokacija').val(),
            DatumPocetka: new Date($('#datumPocetka').val()),
            DatumZavrsetka: new Date($('#datumZavrsetka').val()),
            Mesto: {
                GeografskaDuzina: $('#latlng').val().split(',')[0],
                GeografskaSirina: $('#latlng').val().split(',')[1],
                AdresaNalazenja: ulica + broj + adrArr[1]
            },
            VremeNalazenja: vnDate,
            MaksPutnici: $('#maksPutnici').val(),
            OpisAranzamana: $('#opisAranzmana').val().trim(),
            ProgramPutovanja: $('#programPutovanja').val().trim(),
            SmestajAr: guidForSmestaj,
            Obrisan: false
        };

        let formData = new FormData();
        let img = $('#posterPutovanja')[0];

        if (img.files.length == 0) {
            // u validaciju
            imgPath = null;
            return;
        }

        formData.append('file', img.files[0]);

        document.cookie = window.sessionStorage.getItem('cookie');

        $.ajax({
            url: 'api/Image',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (res) {
                jsonAranzman.PutanjaPostera = res.path;

                // dodaj aranzman
                $.ajax({
                    url: 'api/Aranzmani',
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    headers: {
                        "username": window.sessionStorage.getItem('username')
                    },
                    success: function (res) {
                        alert('Dodat aranzman.');
                        window.location.href = 'Profile.html';
                    },
                    error: function (res) {
                        alert(res.responseText);
                    },
                    data: JSON.stringify(jsonAranzman)
                });

            },
            error: function (res) {
                alert(res.responseText);
                imgPath = null;
            }
        });

    });

});

function validateAranzman()
{
    if ($('#naziv').val().length < 1)
        $('#naziv').attr('border', '1px solid red');
    else
        $('#naziv').removeAttr('border');

    if ($('#lokacija').val().length < 1)
        $('#lokacija').attr('border', '1px solid red');
    else
        $('#lokacija').removeAttr('border');

    let dp = new Date($('#datumPocetka').val());
    let dz = new Date($('#datumZavrsetka').val());
    let vn = new Date($('#vremeNalazenjaDatum').val());

    if (dp > dz || vn > dp) {
        $('#datumPocetka').attr('border', '1px solid red');
        $('#datumZavrsetka').attr('border', '1px solid red');
        $('#vremeNalazenjaDatum').attr('border', '1px solid red');
    }
    else
    {
        $('#datumPocetka').removeAttr('border');
        $('#datumZavrsetka').removeAttr('border');
        $('#vremeNalazenjaDatum').removeAttr('border');
    }

    if ($('#latlng').val().length < 1 || $('#adressText').val().length < 1)
    {
        alert('Izaberite mesto nalazenja.');
    }

    // finish
    
    return true;
}   

function pictureUpload()
{
    let formData = new FormData();
    let img = $('#posterPutovanja')[0];

    if (img.files.length == 0)
    {
        imgPath = null;
        return;
    }
    
    formData.append('file', img.files[0]);

    $.ajax({
        url: 'api/Image',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        success: function (res)
        {
            imgPath = res.path;
        },
        error: function (res)
        {
            alert(res.responseText);
            imgPath = null;
        }
    });
}


function smestajTableShow(jsonArr)
{
    let table = document.createElement('table');
    table.id = 'allSmestaj';
    table.border = '1px solid black';
    table.width = '100%';

    for (let i = 0; i < jsonArr.length; i++) {
        smestajTable(jsonArr[i], table, jsonArr[i].MyGuid);

        let tr = document.createElement('tr');
        let td = document.createElement('td');

        let btnIzaberi = document.createElement('button');
        btnIzaberi.id = 'izaberiSmestaj_' + i;
        btnIzaberi.innerText = 'Izaberi';

        $(document).on('click', '#izaberiSmestaj_' + i, function () {
            $('#floating-panel').children().each(function () {
                this.remove();
            });

            smestajObj = currentSmestajevi[i];

            $('#dodajSmestaj').val('Change');
            $('#smestajNaziv').html(smestajObj.Naziv);

            izabraniSmestaj = currentSmestajevi[i].MyGuid;
            addIzabraniSmestajInfoTable();
        });
        
        td.colSpan = 2;
        td.align = 'center';

        td.append(btnIzaberi);
        tr.append(td);
        table.append(tr);
    }

    $('#floating-panel').append(table);
}

function addIzabraniSmestajInfoTable()
{
    if (izabraniSmestaj) {
        if ($('#izabraniSmestajTable').length)
            $('#izabraniSmestajTable').remove();

        //get semstaj from server
        (new Promise((resolve, reject) => {
            $.ajax({
                url: 'api/Smestaj',
                type: 'GET',
                contentType: 'application/json',
                dataType: 'json',
                data: $.param({ 'guid': izabraniSmestaj }, true),
                success: function (data){
                    resolve(data);
                },
                error: function (error) {
                    reject(error);
                }
            })
        })).then((data) => {
            $('#smestajNaziv').html(data.Naziv);
            let tblSmestaj = document.createElement('table');
            tblSmestaj.id = 'izabraniSmestajTable';
            smestajTable(data, tblSmestaj, "noAr");
            $('#smestajDetails').append(tblSmestaj);
        }).catch((error) => {
            alert(JSON.stringify(error));
        })

    }
}

function fillAr()
{
    if (window.localStorage.getItem('arrangmentToChange')
        && (document.referrer.includes('Profile.html') || document.referrer.includes('AranzmanForm.html') || document.referrer.length == 0)) {
        let arguid = window.localStorage.getItem('arrangmentToChange');
        // get ar from server
        (new Promise((resolve, reject) => {
            $.ajax({
                url: 'api/Aranzmani/GetOne',
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                data: $.param({ 'guid': window.localStorage.getItem('arrangmentToChange') }, true),
                success: function (data) {
                    resolve(data);
                },
                error: function (error) {
                    reject(error);
                }
            })
        })
        ).then((data) => {
            $('#naziv').val(data.Naziv);
            $('#tipAranzmana').val(data.TipAranzmana);
            $('#tipPrevoza').val(data.TipPrevoza);
            $('#lokacija').val(data.LokacijaPutovanja);
            $('#datumPocetka').val(data.DatumPocetka.split('T')[0]);
            $('#datumZavrsetka').val(data.DatumZavrsetka.split('T')[0]);
            $('#latlng').val(data.Mesto.GeografskaDuzina + ',' + data.Mesto.GeografskaSirina);
            $('#adressText').val(data.Mesto.AdresaNalazenja);
            $('#vremeNalazenjaDatum').val(data.VremeNalazenja.split('T')[0]);
            $('#vremeNalazenjaVreme').val(
                new String(data.VremeNalazenja).split('T')[1].split(':').slice(0, 2).join(':')
            );
            $('#maksPutnici').val(data.MaksPutnici);
            $('#opisAranzmana').html(data.OpisAranzamana);
            $('#programPutovanja').html(data.ProgramPutovanja);

            $('#dodajAranzman').val('Save');
            $('#dodajSmestaj').val('Change');

            izabraniSmestaj = data.SmestajAr;

            let img = document.createElement('img');
            img.src = data.PutanjaPostera;
            img.height = 200;
            img.width = 200;
            $('<br/>').insertAfter($('#posterPutovanja'));
            $(img).insertAfter($('#posterPutovanja'));
            $('#posterPutovanja').change(() => img.remove());

            addIzabraniSmestajInfoTable();
        })
        .catch((error) => {
            alert(JSON.stringify(error));
        });

      }
}