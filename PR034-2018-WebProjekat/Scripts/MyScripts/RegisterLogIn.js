﻿
$(document).ready(function () {

    $(document).on('click', '#regBtn', function () {
        
        if ($("table[id='registerTable']").length != 0)
            return;

        if ($("table[id='loginTable']").length != 0)
            $("table[id='loginTable']").remove();
        
        var table = document.createElement("table");
        table.setAttribute("border", "1px solid black");
        table.id = "registerTable";
        // fileds of input
        var tr = document.createElement("tr");
        var th = document.createElement("th");
        var td = document.createElement("td");
        th.innerHTML = "Username:"
        var inptField = document.createElement("input");
        inptField.setAttribute("type", "text");
        inptField.setAttribute("name", "Username");
        inptField.setAttribute("id", "Username");
        td.append(inptField);
        tr.append(th);
        th.after(td);
        table.append(tr);

        var tr = document.createElement("tr");
        var th = document.createElement("th");
        var td = document.createElement("td");
        th.innerHTML = "Password:"
        var inptField = document.createElement("input");
        inptField.setAttribute("type", "password");
        inptField.setAttribute("name", "Password");
        inptField.setAttribute("id", "Password");
        td.append(inptField);
        tr.append(th);
        th.after(td);
        table.append(tr);

        var tr = document.createElement("tr");
        var th = document.createElement("th");
        var td = document.createElement("td");
        th.innerHTML = "Name:"
        var inptField = document.createElement("input");
        inptField.setAttribute("type", "text");
        inptField.setAttribute("name", "Name");
        inptField.setAttribute("id", "Name");
        td.append(inptField);
        tr.append(th);
        th.after(td);
        table.append(tr);

        var tr = document.createElement("tr");
        var th = document.createElement("th");
        var td = document.createElement("td");
        th.innerHTML =  "Surname:"
        var inptField = document.createElement("input");
        inptField.setAttribute("type", "text");
        inptField.setAttribute("name", "Surname");
        inptField.setAttribute("id", "Surname");
        td.append(inptField);
        tr.append(th);
        th.after(td);
        table.append(tr);

        var tr = document.createElement("tr");
        var th = document.createElement("th");
        var td = document.createElement("td");
        th.innerHTML =  "Email:"
        var inptField = document.createElement("input");
        inptField.setAttribute("type", "email");
        inptField.setAttribute("name", "Email");
        inptField.setAttribute("id", "Email");
        td.append(inptField);
        tr.append(th);
        th.after(td);
        table.append(tr);

        var tr = document.createElement("tr");
        var th = document.createElement("th");
        var td = document.createElement("td");
        th.innerHTML = "Gender:"
        var selField = document.createElement("select");
        selField.setAttribute("name", "Gender");
        selField.setAttribute("id", "Gender");
        var opt1 = document.createElement("option");
        var opt2 = document.createElement("option");
        opt1.value = 'M';
        opt1.text = 'M';
        opt2.value = 'F';
        opt2.text = 'F';
        selField.appendChild(opt1);
        selField.appendChild(opt2);
        td.append(selField);
        tr.append(th);
        th.after(td);
        table.append(tr);

        var tr = document.createElement("tr");
        var th = document.createElement("th");
        var td = document.createElement("td");
        th.innerHTML = "Date of Birth:"
        var inptField = document.createElement("input");
        inptField.setAttribute("type", "date");
        inptField.setAttribute("name", "BirthDate");
        inptField.setAttribute("id", "BirthDate");

        var dt = new Date();

        var dd = dt.getDate();
        if (dd < 10) dd = '0' + dd;
        dd = String(dd);

        var mm = dt.getMonth() + 1;
        if (mm < 10) mm = '0' + mm;
        mm = String(mm);
        var yy = String(dt.getFullYear());

        var today = yy + '-' + mm + '-' + dd ;
        inptField.setAttribute("max", today);
        td.append(inptField);
        tr.append(th);
        th.after(td);
        table.append(tr);

        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.setAttribute("colspan", "2");
        td.align = "center";
        var inptField = document.createElement("input");
        inptField.value = "Register";
        inptField.setAttribute("type", "submit");
        inptField.setAttribute("id", "registerBtn");
        
        td.append(inptField);
        tr.append(td);
        table.append(tr);

        //

        $("#formDiv").append(table);
    });

    $(document).on('click', '#logInBtn', function () {
        if ($("table[id=loginTable]").length != 0)
            return

        if ($("table[id=registerTable]").length != 0)
            $("table[id=registerTable]").remove();

        var table = document.createElement("table");
        table.setAttribute("border", "1px solid black");
        table.id = "loginTable";
        // fileds of input
        var tr = document.createElement("tr");
        var th = document.createElement("th");
        var td = document.createElement("td");
        th.innerHTML = "Username:"
        var inptField = document.createElement("input");
        inptField.setAttribute("type", "text");
        inptField.setAttribute("id", "Username");

        td.append(inptField);
        tr.append(th);
        th.after(td);
        table.append(tr);

        var tr = document.createElement("tr");
        var th = document.createElement("th");
        var td = document.createElement("td");
        th.innerHTML = "Password:"
        var inptField = document.createElement("input");
        inptField.setAttribute("type", "password");
        inptField.setAttribute("id", "Password");
        td.append(inptField);
        tr.append(th);
        th.after(td);
        table.append(tr);

        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.setAttribute("colspan", "2");
        td.align = "center";
        var inptField = document.createElement("input");
        inptField.setAttribute("type", "submit");
        inptField.setAttribute("id", "logBtn");
        td.append(inptField);
        tr.append(td);
        table.append(tr);

        $("#formDiv").append(table);
    });

});
