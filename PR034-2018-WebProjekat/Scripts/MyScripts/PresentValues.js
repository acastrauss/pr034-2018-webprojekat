﻿// methods to present values in tables, lists, etc.

function rezervacijaTable(rezervacija, table)
{
    (new Promise((resolve, reject) => {
        $.ajax({
            url: 'api/Aranzmani/GetOne',
            type: 'GET',
            data: $.param({ 'guid': rezervacija.AranzmanRez }, true),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                resolve(data);
            },
            error: function (data) {
                reject(data);
            }
        })
    }).then((data) => {

        if (data) {
            let trAr = document.createElement('tr');
            let tdAr = document.createElement('td');
            let tblAr = document.createElement('table');
            tblAr.border = '1px solid black';
            trAr.append(tdAr);
            tdAr.append(tblAr);
            table.append(trAr);

            aranzmanBasicTable(data, tblAr);
            aranzmanMoreInfoTable(data, tblAr, false);

            $.ajax({
                url: 'api/SmestajneJedinice/GetOne',
                type: 'GET',
                data: $.param({ 'guid': rezervacija.SmestajnaJedinicaRez }, true),
                dataType: 'json',
                contentType: 'application/json',
                success: function (dataSj) {

                    let divSj = document.createElement('div');
                    divSj.id = rezervacija.SmestajnaJedinicaRez + '_' + rezervacija.MyGuid;
                    $(divSj).html('Vasa smestajna jedinica:<br/>' +
                        'Broj gostiju:' + dataSj.BrojGostiju + '<br/>' +
                        'Cena:' + dataSj.Cena + '<br/>' +
                        (dataSj.PetFriendly ? 'Pet friendly' : '') + '<br/>');

                    
                    tdAr.append(divSj);

                    let tdBtn = document.createElement('td');
                    tdAr.append(tdBtn);

                    // add button to aranzmanTable
                    if (rezervacija.StatusRezervacije == 0) {
                        let sjChangeBtn = document.createElement('button');
                        sjChangeBtn.id = rezervacija.SmestajnaJedinicaRez + '_Cancel';
                        tdBtn.append(sjChangeBtn);
                        // check
                        if (new Date(data.DatumZavrsetka.split('T')) < new Date() && rezervacija.KomentarGuid == emptyGuid) {
                            // komentar
                            sjChangeBtn.innerText = 'Leave a comment';
                            sjChangeBtn.onclick = function () {
                                let tdCommentForm = document.createElement('td');
                                tdAr.append(tdCommentForm);

                                let commText = document.createElement('textarea');
                                commText.id = rezervacija.MyGuid + '_text';
                                commText.type = 'text';
                                tdCommentForm.append('Text:');
                                tdCommentForm.append(document.createElement('br'));
                                tdCommentForm.append(commText);
                                tdCommentForm.append(document.createElement('br'));

                                let selectList = document.createElement('select');
                                selectList.id = rezervacija.MyGuid + '_grade';
                                tdCommentForm.append('Grade:');
                                tdCommentForm.append(document.createElement('br'));
                                tdCommentForm.append(selectList);
                                tdCommentForm.append(document.createElement('br'));
                                
                                for (let i = 1; i < 6; i++) {
                                    let option = document.createElement("option");
                                    option.value = i;
                                    option.text = i;
                                    selectList.appendChild(option);
                                };
                                tdCommentForm.append(document.createElement('br'));

                                let submitComm = document.createElement('button');
                                submitComm.id = rezervacija.MyGuid + '_submit';
                                submitComm.innerText = 'Save comment';
                                tdCommentForm.append(submitComm);

                                submitComm.onclick = function () {
                                    let textArea = $(commText).val();
                                    if (textArea.length == 0) {
                                        $(commText).css({   
                                            'border-color': 'red',
                                            'border-width': '1px',
                                            'border-style': 'solid'
                                        });
                                    }
                                    else {
                                        $(commText).css({
                                            'border-color': '',
                                            'border-width': '',
                                            'border-style': ''
                                        });

                                        $.ajax({
                                            url: 'api/Komentar',
                                            type: 'POST',
                                            data: JSON.stringify({
                                                'TuristaKom': window.sessionStorage.getItem('cookie'),
                                                'AranzmanKom': rezervacija.AranzmanRez,
                                                'Tekst': $(commText).val(),
                                                'Ocena': $(selectList).val(),
                                                'Odobren': false
                                            }),
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            success: function (data) {
                                                alert('Comment added!');
                                                tdBtn.remove();
                                                tdCommentForm.remove();
                                            },
                                            error: function (error) {
                                                alert(JSON.stringify(error));
                                            }
                                        });
                                    }
                                };
                            };
                        }
                        else if (new Date(data.VremeNalazenja.split('T')) > new Date()) {
                            // otkazi
                            sjChangeBtn.innerText = 'Cancel reservation';
                            sjChangeBtn.onclick = function () {
                                $.ajax({
                                    url: 'api/Rezervacije/Cancel',
                                    type: 'POST',
                                    data: JSON.stringify({
                                        'Reservation': rezervacija.MyGuid,
                                        'Sj': rezervacija.SmestajnaJedinicaRez
                                    }),
                                    dataType: 'json',
                                    contentType: 'application/json',
                                    success: function (data) {
                                        alert('Canceled:' + data);
                                        tdBtn.childNodes.forEach(x => x.remove());
                                        tdBtn.innerHTML = 'Canceled';
                                    },
                                    error: function (error) {
                                        alert(JSON.stringify(error));
                                    }
                                });
                            };
                        }
                    }
                    else {
                        // canceled
                        tdBtn.innerHTML = 'Canceled';
                    }
                },
                error: function (errorSj) {
                    alert(JSON.stringify(data));
                }
            });
        };
    }).catch((error) => {
        alert(JSON.stringify(error));
    }));
}

function initMap() {
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 8,
        center: { lat: 40.731, lng: -73.997 },
    });
    const geocoder = new google.maps.Geocoder();
    const infowindow = new google.maps.InfoWindow();

    $('.moreInfoBtn').click(function () {
        geocodeLatLng(geocoder, map, infowindow, this.id.split('_')[1]);
    });
}

function geocodeLatLng(geocoder, map, infowindow, arGuid) {
    const input = $('#latlng_' + arGuid).html();
    const latlngStr = input.split(",", 2);
    const latlng = {
        lat: parseFloat(latlngStr[0]),
        lng: parseFloat(latlngStr[1]),
    };

    geocoder
        .geocode({ location: latlng })
        .then((response) => {
            if (response.results[0]) {
                map.setZoom(11);

                const marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                });

                infowindow.setContent(response.results[0].formatted_address);
                infowindow.open(map, marker);
            } else {
                window.alert("No results found");
            }
        })
        .catch((e) => window.alert("Geocoder failed due to: " + e));
}

function mestoNalazenjaTable(mestoNalazenja, table, arGuid)
{
    let tr = document.createElement('tr');
    let th = document.createElement('th');
    let td = document.createElement('td');


    th.innerHTML = 'Mesto Nalazenja:';
    tr.append(th);
    tr.append(td);
    table.append(tr);

    td.innerHTML = mestoNalazenja.AdresaNalazenja + '<br/>' +
        mestoNalazenja.GeografskaDuzina + ',' + mestoNalazenja.GeografskaSirina;

    if ($('#map').length == 0) {
        // css za reverse geocoding
        let cssReverseGeocoding = document.createElement('link')
        cssReverseGeocoding.rel = 'stylesheet';
        cssReverseGeocoding.type = 'text/css';
        cssReverseGeocoding.href = 'Content/MyContent/ReverseGeocoding.css';
        document.head.append(cssReverseGeocoding);

        // vrednosti neophodne da se pokrene geocoding u mapi
        let tdHiddenBtns = document.createElement('td');
        tr.append(tdHiddenBtns);
        let divHidden = document.createElement('div')
        divHidden.hidden = true;
        tdHiddenBtns.append(divHidden);
        let longLat = document.createElement('p');
        longLat.id = 'latlng_' + arGuid;
        longLat.innerHTML = mestoNalazenja.GeografskaDuzina + ',' + mestoNalazenja.GeografskaSirina;
        divHidden.append(longLat);

        // za mapu
        let divMap = document.createElement('div');
        divMap.id = 'map';
        divMap.style.overflow = 'visible';
        let apiScript = document.createElement('script');
        apiScript.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDXkWVLOUazhQCiVEUpEjonfu1SCwVJZRM&callback=initMap&v=weekly";
        apiScript.async = true;

        $(divMap).insertAfter('#aranzmaniTable');
        $(apiScript).insertAfter(divMap);

    }
}

function smestajTable(smestaj, table, arGuid, showSj = true)
{
    if (typeof smestaj == 'string' || smestaj instanceof String)
        smestaj = JSON.parse(smestaj);

    if (smestaj.Obrisan) return;

    //arName = arName.replaceAll(' ', '_');

    let tr = document.createElement('tr');
    let th = document.createElement('th');
    let td = document.createElement('td');

    tr.append(th);
    tr.append(td);
    table.append(tr);

    th.innerHTML = 'Smestaj:';

    td.id = arGuid + '_Smestaj';

    td.innerHTML = tip_smestaja[smestaj.TipSmestaja] + ', ' + smestaj.Naziv + '<br/>' +
        'Detalji:' + '<br/>' +
        (smestaj.Zvezdice ? 'Zvedice: ' + smestaj.Zvezdice + '<br/>' : '') +
        (smestaj.Bazen ? 'Bazen' + '<br/>' : '')  +
        (smestaj.SpaCentar ? 'Spa Centar' + '<br/>' : '')  +
        (smestaj.Invaliditet ? 'Osposobljeno za osobe sa invaliditetom' + '<br/>': '')  +
        (smestaj.Wifi ? 'Wifi' + '<br/>' : '') ;

    // smestajne jedinice
    if (!smestaj.SmestajneJedinice) return;


    let sjArr;

    if (!showSj) return;

    td.innerHTML += 'Smestajne jedinice:' + '<br/>';

    $.ajax({
        url: 'api/SmestajneJedinice/GetMultiple',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        data: $.param({ 'guids': smestaj.SmestajneJedinice}, true),
        success: function(result) {
            sjArr = result;
        },
        error: function (result) {
            alert(result.responseText);
        }
    }).then(function () {
        let i = 0;
        let minCena = Number.MAX_SAFE_INTEGER;

        sjArr.forEach(function (sj) {

            if (sj.Obrisana) return;

            if (sj.Cena < minCena) minCena = sj.Cena;

            let listSj = document.createElement('ul');
            listSj.id = sj.MyGuid;//'listSj_' + smestaj.MyGuid + '_' + i;
            listSj.hidden = true;

            let li1 = document.createElement('li');
            li1.innerHTML = 'Broj Gostiju:' + sj.BrojGostiju;
            listSj.append(li1);

            let li3 = document.createElement('li');
            li3.innerHTML = 'Cena:' + sj.Cena + '\u20AC';
            listSj.append(li3);

            let li2 = document.createElement('li');
            li2.innerHTML = (sj.PetFriendly ? 'Pet Friendly' : '');

            if (sj.PetFriendly)
                listSj.append(li2);
            else
                li2.remove();

            let li4 = document.createElement('li');
            li4.innerHTML = (sj.Rezervisana ? 'Rezervisana' : 'Slobodna');
            listSj.append(li4);

            let btn = document.createElement('button');

            btn.id = sj.MyGuid;//'btnSj_' + smestaj.MyGuid + '_' + i;
            btn.innerText = i + 1;
            $(btn).click(function (e) {
                e.preventDefault();

                if ($(listSj).attr('hidden')) {
                    $(listSj).attr('hidden', false);
                }
                else {
                    $(listSj).attr('hidden', true);
                }
            });

            td.append(btn);
            td.append(listSj);

            // only show reserve button on homepage
            if (!sj.Rezervisana && window.location.href.includes('Index.html') && 
                window.sessionStorage.getItem('cookie') && window.sessionStorage.getItem('role') == 'Turista') {
                let btnRes = document.createElement('button');
                btnRes.id = sj.MyGuid;//'resSj_' + smestaj.MyGuid + '_' + i;
                btnRes.innerText = 'Reserve';
                $(btnRes).click(function (e) {
                    e.preventDefault();

                    $.ajax({
                        url: 'api/SmestajneJedinice/Reserve',
                        type: 'POST',
                        dataType: 'json',
                        contentType: 'application/json',
                        data: JSON.stringify({
                            'TuristaRez': window.sessionStorage.getItem('cookie'),
                            'StatusRezervacije': 0,
                            'AranzmanRez': arGuid,
                            'SmestajnaJedinicaRez': sj.MyGuid
                        }),
                        success: function (results) {
                            alert('You reserved!');
                            sj.Rezervisana = true;
                            li4.innerHTML = 'Reserved';
                            btnRes.remove();
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });
                });

                $(btnRes).insertAfter(btn);
                $('<br/>').insertAfter(btnRes);
            }

            i = i + 1;
        });

        $('#minCena_' + arGuid).html('Od:' + minCena + '\u20AC');
    });
}

// only basic information about it
function aranzmanBasicTable(aranzman, table)
{
    if (aranzman.Obrisan) return;   
    // dodaj da th bude naziv atributa a td podatak (vrv input)
    let tr = document.createElement('tr');
    let th1 = document.createElement('th');
    let th2 = document.createElement('th');
    let th3 = document.createElement('th');
    let th4 = document.createElement('th');
    let th5 = document.createElement('th');

    th1.innerHTML = 'Naziv:';
    th2.innerHTML = 'Lokacija:';
    th3.innerHTML = 'Polazak:';
    th4.innerHTML = 'Povratak:';
    th5.innerHTML = 'Cena:';

    tr.append(th1);
    tr.append(th2);
    tr.append(th3);
    tr.append(th4);
    tr.append(th5);

    table.append(tr);

    let tr2 = document.createElement('tr');
    tr2.id = aranzman.MyGuid + '_MainInfo';
    let td1 = document.createElement('td');
    let td2 = document.createElement('td');
    let td3 = document.createElement('td');
    let td4 = document.createElement('td');
    let td5 = document.createElement('td');
    let td6 = document.createElement('td');

    td1.innerHTML = aranzman.Naziv;
    tr2.append(td1);

    td2.innerHTML = aranzman.LokacijaPutovanja;
    tr2.append(td2);

    let dt1 = document.createElement('input');
    dt1.type = 'date';
    dt1.readOnly = true;
    dt1.value = String(aranzman.DatumPocetka).split('T')[0];
    td3.append(dt1);
    tr2.append(td3);

    let dt2 = document.createElement('input');
    dt2.type = 'date';
    dt2.readOnly = true;
    dt2.value = String(aranzman.DatumZavrsetka).split('T')[0];
    td4.append(dt2);
    tr2.append(td4);

    td5.id = 'minCena_' + aranzman.MyGuid;
    tr2.append(td5);

    var img = new Image(100, 100);
    img.src = aranzman.PutanjaPostera;
    img.alt = aranzman.LokacijaPutovanja;
    td6.append(img);
    tr2.append(td6);

    let td7 = document.createElement('td');
    let moreBtn = document.createElement('button');
    moreBtn.innerText = 'Prikazi vise';
    moreBtn.className = 'moreInfoBtn';
    moreBtn.id = 'moreInfoBtn_' + aranzman.MyGuid;
    td7.append(moreBtn);
    tr2.append(td7);

    table.append(tr2);
}

function aranzmanMoreInfoTable(aranzman, table, showSj = true)
{
    if (aranzman.Obrisan) return;

    table.id = aranzman.MyGuid;
        
    let tr = document.createElement('tr');
    tr.className = 'aranzmanMoreInfo';
    tr.hidden = true;

    let tr22 = document.createElement('tr');
    tr22.className = 'aranzmanMoreInfo';
    tr22.hidden = true;

    let th1 = document.createElement('th');
    let th2 = document.createElement('th');
    let th3 = document.createElement('th');
    let th4 = document.createElement('th');
    let th5 = document.createElement('th');
    let th6 = document.createElement('th');
    let th7 = document.createElement('th');
    
    th1.innerHTML = 'Tip aranzmana:';
    th2.innerHTML = 'Tip prevoza:';
    th3.innerHTML = 'Maks putnici:';
    th4.innerHTML = 'Opis:';
    th5.innerHTML = 'Program:';
    th6.innerHTML = 'Vreme nalazenja:';
    th7.innerHTML = 'Mesto nalazenja:';
    
    tr.append(th1);
    tr.append(th2);
    tr.append(th3);
    tr.append(th4);
    tr.append(th5);

    table.append(tr);
    
    let tr2 = document.createElement('tr');
    tr2.className = 'aranzmanMoreInfo';
    tr2.hidden = true;

    let tr23 = document.createElement('tr');
    tr23.className = 'aranzmanMoreInfo';
    tr23.hidden = true;

    let td1 = document.createElement('td');
    td1.innerHTML = tip_aranzmana[aranzman.TipAranzmana];

    let td2 = document.createElement('td');
    td2.innerHTML = tip_prevoza[aranzman.TipPrevoza];

    let td3 = document.createElement('td');
    td3.innerHTML = aranzman.MaksPutnici;

    let td4 = document.createElement('td');
    td4.innerHTML = aranzman.OpisAranzamana;

    let td5 = document.createElement('td');
    td5.innerHTML = aranzman.ProgramPutovanja;

    let td6 = document.createElement('td');
    let dt1 = document.createElement('input');
    dt1.type = 'datetime';
    dt1.readOnly = true;
    
    let date = new Date(String(aranzman.VremeNalazenja));

    dt1.value = Intl.DateTimeFormat('en-GB').format(date) + ' ' + date.getHours() + ':' + date.getMinutes();
    td6.append(dt1);
    let td7 = document.createElement('td');
    let tbl1 = document.createElement('table');
    tbl1.id = 'MestoNalazenjaTable_' + aranzman.MyGuid;

    mestoNalazenjaTable(aranzman.Mesto, tbl1, aranzman.MyGuid);
    td7.append(tbl1);

    tr2.append(td1);
    tr2.append(td2);
    tr2.append(td3);
    tr2.append(td4);
    tr2.append(td5);
    tr23.append(td6);
    tr23.append(td7);

    table.append(tr2);

    tr22.append(th6);
    tr22.append(th7);
    table.append(tr22);

    table.append(tr23);

    let tr3 = document.createElement('tr');
    let td8 = document.createElement('td');
    let tbl2 = document.createElement('table');
    tbl2.id = 'SmestajTable';
    let smestajAr;
    
    $.ajax({
        url: 'api/Smestaj',
        type: 'GET',
        data: $.param({ 'guid': aranzman.SmestajAr}, true),
        dataType: 'text',
        success: function (res) {
            smestajAr = JSON.parse(res);
            smestajTable(smestajAr, tbl2, aranzman.MyGuid, showSj);
        },
        error: function (res) {
            alert(res.responseText);
        }
    }).done(() => {
    });

    td8.append(tbl2);
    tr3.append(td8);
    tr3.className = 'aranzmanMoreInfo';
    tr3.hidden = true;
   
    table.append(tr3);
}