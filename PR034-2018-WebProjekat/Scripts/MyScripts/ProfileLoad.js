﻿    
$(document).ready(function () {

    $.ajax({
        url: 'api/Profile',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (result)
        {
            var json = JSON.parse(result);
            $('#Username').val(json.KorisnickoIme);
            $('#Password').val(json.Lozinka);
            $('#Name').val(json.Ime);
            $('#Surname').val(json.Prezime);
            $('#Gender').val(json.Pol);
            $('#Email').val(json.Email);
            $('#BirthDate').val(String(json.DatumRodjenja).split('T')[0]);

            switch (json.UlogaKorisnika) {
                case 0:
                    $('#Role').val(json.UlogaKorisnika);
                    break;
                case 1:
                    $('#Role').val(json.UlogaKorisnika);
                    managerProfile(json);
                    break;
                case 2:
                    $('#Role').val(json.UlogaKorisnika);
                    turistaProfile(json);
                    break;
                default:
            }    
        },
        error: function (result)
        {
            var json = JSON.parse(result.responseText);
            alert(json['Message']);
            window.location.href = '/Index.html';
        },
        data: {
            UserGUID: window.sessionStorage.getItem('cookie')
        }
    });

    window.document.cookie = window.sessionStorage.getItem('cookie');

    $(document).on('click', '#saveProfile', function (e) {
        e.preventDefault();
        if (!validateReg()) return;

        $.ajax({
            url: 'api/Profile',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            success: function (result) {
                alert('Changed profile.');
                location.reload();
            },
            error: function (result) {
                var json = JSON.parse(result.responseText);
                alert(json['Message']);
                window.location.href = '/Index.html';
            },
            data: JSON.stringify(
                {
                    KorisnickoIme: $('#Username').val(),
                    Lozinka: $('#Password').val(),
                    Ime: $('#Name').val(),
                    Prezime: $('#Surname').val(),
                    Pol: $('#Gender').val(),
                    Email: $('#Email').val(),
                    UlogaKorisnika: $('#Role').val(),
                    Cookie: window.sessionStorage.getItem('cookie'),
                }
            )
        });
    });
});

function managerProfile(json)
{
    $('#searchSortAranzmani').show();
    $('#searchSortAranzmani').load('SortSearchAranzmani.html');

    var divider = document.createElement('div');
    divider.className = 'divider';
    divider.id = 'tableDiv';
    $(divider).insertAfter('#searchSortAranzmani');
    var p = document.createElement('p');
    p.innerHTML = 'Vasi aranzmani:'
    p.id = 'pAr';
    $(p).insertAfter(divider);

    let tbl = document.createElement('table');
    tbl.id = 'aranzmaniTable';
    tbl.title = 'Vasi Aranzmani:';
    tbl.border = '1px solid black';
    $(tbl).insertAfter('#pAr');

    window.sessionStorage.setItem('myArrangements', json.Aranzmani);

    (new Promise((resolve, reject) => {
        $.ajax({
            url: 'api/Aranzmani/GetMultiple',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            data: $.param({ 'guids': json.Aranzmani }, true),
            success: function (data) {
                resolve(data);
            },
            error: function (error) {
                reject(error);
            },
        })
    }
    ))
        .then((data) => {

            allArrangmenets = [...data];
            toShowArrangements = [...data];

            arrProfilePresent(data, tbl);

        })
        .catch((error) => {
            alert(error);
        });


    $.ajax({
        url: 'api/Rezervacije/GetReservationsForManager',
        type: 'GET',
        data: $.param({ guid: window.sessionStorage.getItem('cookie') }, true),
        success: function (data) {
            
            data.forEach(r => {
                let tr = document.createElement('tr');
                let td = document.createElement('td');
                let table = document.createElement('table');
                $('#reservationsForMyArs').show();
                $('#myArsReservationsTable').append(tr);
                tr.append(td);
                td.append(table);

                table.id = r.MyGuid;
            
                rezervacijaTable(r, table);
            });
            

        }
    });
}

function arrProfilePresent(arrangmenets, tbl) {
    arrangmenets.forEach((a) => {
        let tr = document.createElement('tr');
        let td = document.createElement('td');
        let tbl2 = document.createElement('table');

        aranzmanBasicTable(a, tbl2);
        let tdar = document.createElement('td');
        tdar.innerHTML = "<td><td/>";
        let btnChange = document.createElement('button');
        btnChange.id = 'change_' + a.MyGuid;
        btnChange.innerHTML = "Change";
        $(btnChange).click(function (e) {
            e.preventDefault();

            window.localStorage.setItem('arrangmentToChange', a.MyGuid);
            window.location.href = '/AranzmanForm.html';
        });

        tdar.append(btnChange);
        tbl2.childNodes[1].appendChild(tdar);

        let tdDel = document.createElement('td');
        let btnDel = document.createElement('button');
        btnDel.id = 'delete_' + a.MyGuid;
        btnDel.innerHTML = 'Delete';

        $(btnDel).click(function (e) {
            e.preventDefault();

            $.ajax({
                url: 'api/Aranzmani/DeleteGuid' + '?'
                    + $.param({ 'guid': a.MyGuid }, true),
                type: 'DELETE',
                dataType: 'json',
                contentType: 'application/json',
                success: function (result) {
                    alert('Deleted ' + a.Naziv);
                    $(tr).remove();
                },
                error: function (result) {
                    alert(result.responseText);
                }
            });
        });

        tdDel.append(btnDel);
        tbl2.childNodes[1].appendChild(tdDel);

        aranzmanMoreInfoTable(a, tbl2);
        td.append(tbl2);
        tr.append(td);
        tbl.append(tr);
    });
}

function turistaProfile(json) {
    if (json.Rezervacije.length == 0) return;

    var divider = document.createElement('div');
    divider.className = 'divider';
    divider.id = 'tableDiv';
    $(divider).insertAfter('#generalInfo');
    var p = document.createElement('p');
    p.innerHTML = 'Your reservations:'
    p.id = 'pAr';
    $(p).insertAfter(divider);

    let tbl = document.createElement('table');
    tbl.title = 'Your reservations:';
    tbl.border = '1px solid black';
    $(tbl).insertAfter('#pAr');

    (new Promise((resolve, reject) => {
        $.ajax({
            url: 'api/Rezervacija/GetMultiple',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            data: $.param({ 'guids': json.Rezervacije }, true),
            success: function (data) {
                resolve(data);
            },
            error: function (error) {
                reject(error);
            },
        })
    }
    ))
        .then((data) => {
            data.forEach((x) => {
                let tr = document.createElement('tr');
                let td = document.createElement('td');
                let tblOneRez = document.createElement('table');
                tblOneRez.border = '1px solid black';
                tbl.append(tr);
                tr.append(td);
                td.append(tblOneRez);

                rezervacijaTable(x, tblOneRez);
            });
        })
        .catch((error) => {
            alert(error);
        })
}