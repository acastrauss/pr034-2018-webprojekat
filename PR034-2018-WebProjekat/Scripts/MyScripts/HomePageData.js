﻿
var allArrangmenets = [];
var toShowArrangements = [];

$(document).ready(function () {

    $('#allCommentsBtn').click(() => { window.location.href = '/AllComments.html' });

    $('#searchSortAranzmani').load('SortSearchAranzmani.html');


    $.ajax({
        url: 'api/Aranzmani/GetAll',
        type: 'GET',
        success: function (result) {

            let availableAr = false;

            result[0].forEach((arIndx) => {
                let ar = JSON.parse(arIndx);
                if (new Date(String(ar.VremeNalazenja)) < new Date()) return;

                allArrangmenets.push(ar);

                if (!availableAr)
                {
                    let p = document.createElement('p');
                    p.innerHTML = 'Dostupni aranzmani:';
                    $('#arTitle').append(p);
                    availableAr = true;
                }
            });

            allArrangmenets.sort((a, b) => new Date(String(a.DatumPocetka)) - new Date(String(b.DatumPocetka)) )
                .forEach((arObj) => {
                    (function (arrT) {
                        let tbl = document.createElement('table');
                        aranzmanBasicTable(arrT, tbl);
                        aranzmanMoreInfoTable(arrT, tbl);
                        let tr = document.createElement('tr');
                        let td = document.createElement('td');
                        td.append(tbl);
                        tr.append(td);
                        $('#aranzmaniTable').append(tr);
                    })(arObj);
                });

            toShowArrangements = [...allArrangmenets];
        },
        error: function (result) {
            alert(result.responseText);
        }
    });

    // if role == Menadzer create notification for comments
    // if role == Administrator create notification for possible blocked users
    createNotificationBtn();

    if (window.sessionStorage.getItem('role') == 'Menadzer') {

        let btnDodaj = document.createElement('button');
        btnDodaj.id = 'btnAddAr';
        btnDodaj.innerText = 'Dodaj aranzman';
        btnDodaj.className = 'regButton';
        $('#addBtns').append(btnDodaj);
        btnDodaj.onclick = function () {
            window.location.href = '/AranzmanForm.html';
        };

        let div2 = document.createElement('div');
        div2.className = 'divider';
        $('#addBtns').append(div2);

        let btnAccom = document.createElement('button');
        btnAccom.className = 'regButton';
        btnAccom.id = 'changeSmestaj';
        btnAccom.innerText = 'Accommodations management';
        $('#accomDiv').append(btnAccom);
        btnAccom.onclick = function () {

            if ($('#accomBtns').length == 0) {
                let div1 = document.createElement('div');
                div1.id = 'accomBtns';
                $('#accomDiv').append(div1);

                let btnChange = document.createElement('button');
                btnChange.id = 'changeSmestajeve';
                btnChange.innerText = 'Change accommodations';
                btnChange.className = 'regButton';
                $(div1).append(btnChange);
                btnChange.onclick = function () {
                    window.location.href = '/SviSmestajevi.html';
                };

                let btnDodaj2 = document.createElement('button');
                btnDodaj2.id = 'btnAddSmestaj';
                btnDodaj2.innerText = 'Add accommodations';
                btnDodaj2.className = 'regButton';
                $(div1).append(btnDodaj2);
                btnDodaj2.onclick = function () {
                    window.location.href = '/SmestajForm.html';
                };
            }
            else
                $('#accomBtns').remove();
        };
    }
    else {
        let btnAccom = document.createElement('button');
        btnAccom.className = 'regButton';
        btnAccom.id = 'changeSmestaj';
        btnAccom.innerText = 'Pregled smestaja';
        $('#accomDiv').append(btnAccom);
        btnAccom.onclick = function () {
            window.location.href = '/SviSmestajevi.html';
        };
    }

    if (window.sessionStorage.getItem('role') == 'Administrator') {
        $('#allUsersDiv').show();

        $('#allUsersBtn').click(() => location.href = 'AllUsers.html');
    }    

    $(document).on('click', '#oldAranzmani', function () {
        window.location.href = '/OldArrangements.html';
    });

});

function createNotificationBtn() {
    if (window.sessionStorage.getItem('role') == 'Menadzer') {
        // create notification for comments
        $('#notificationsText').html('Komentari');
        $('#notificationsLink').attr('href', 'Comments.html');

        $.ajax({
            url: 'api/Komentar/GetCommentsNumberForManager',
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            data: $.param({ 'guid': window.sessionStorage.getItem('cookie') }, true),
            success: function (data) {
                $('#notificationsBadge').html(data);
            },
            error: function (error) {
                alert(JSON.stringify(error));
            }
        });
    }
    else if (window.sessionStorage.getItem('role') == 'Administrator') {
        // notification for blocked users
        $('#notificationsText').html('Korisnici za blokiranje');
        $('#notificationsLink').attr('href', 'UsersToBlock.html');

        $.ajax({
            url: 'api/Users/GetNumberPotentialToBlockUsers',
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            data: $.param({ 'guid': window.sessionStorage.getItem('cookie') }, true),
            success: function (data) {
                $('#notificationsBadge').html(data);
            },
            error: function (error) {
                alert(JSON.stringify(error));
            }
        });
    }
    else {
        $('#notificationsDiv').remove();
    }
}