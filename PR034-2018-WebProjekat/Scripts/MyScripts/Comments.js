﻿$(document).ready(function () {
    if (!window.location.href.includes('AllComments.html')) {
        (new Promise((resolve, reject) => {
            $.ajax({
                url: 'api/Komentar/GetCommentsForManager',
                type: 'GET',
                contentType: 'application/json',
                dataType: 'json',
                data: $.param({ 'guid': window.sessionStorage.getItem('cookie') }, true),
                success: function (data) {
                    resolve(data);
                },
                error: function (error) {
                    reject(error);
                }
            })
        })).then((data) => {
            processComments(data, true);
        }).catch((error) => {
            alert(JSON.stringify(error));
        });
    }
    else {
        (new Promise((resolve, reject) => {
            $.ajax({
                url: 'api/Komentar/GetAll',
                type: 'GET',
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    resolve(data);
                },
                error: function (error) {
                    reject(error);
                }
            })
        })).then((data) => {
            processComments(data, false);
        }).catch((error) => {
            alert(JSON.stringify(error));
        });
    }
});

function processComments(data, managerUsing) {
    let tableId = window.location.href.includes('AllComments.html') ? 
        'allCommentsTable':
        'managCommentsTable';

    if (data.length != 0) createCommentsHeader(tableId);

    data.forEach(x => {

        // if it's all comments only include approved ones
        if (!x.Odobren && !window.location.href.includes('Comments.html')) return;

        let commentRow = document.createElement('tr');

        let tdAr = document.createElement('td');
        let tdTur = document.createElement('td');
        let tdKom = document.createElement('td');
        let tdBtn = document.createElement('td');

        $('#' + tableId).append(commentRow);
        commentRow.append(tdAr);
        commentRow.append(tdTur);
        commentRow.append(tdKom);
        commentRow.append(tdBtn);

        // aranzman setup
        let tableAr = document.createElement('table');
        tableAr.id = x.MyGuid + '_' + x.AranzmanKom;
        tdAr.append(tableAr);
        addAranzmanComment(x.AranzmanKom, tableAr);

        // turista setup
        let tableTur = document.createElement('table');
        tableTur.id = x.MyGuid + '_' + x.TuristaKom;
        tdTur.append(tableTur);
        addTuristaComment(x.TuristaKom, tableTur, managerUsing);

        // komentar setup
        let tableKom = document.createElement('table');
        tableKom.id = x.MyGuid;
        tdKom.append(tableKom);
        addKomentarComment(x, tableKom);

        // odobren btn
        if (!x.Odobren) {
            if (!x.Zabranjen) {
                let odobrenBtn = document.createElement('button');
                odobrenBtn.id = x.MyGuid + '_' + 'odobrenBtn';
                odobrenBtn.innerHTML = 'Odobri';
                odobrenBtn.onclick = () => { odobriKomentar(x.MyGuid, tdBtn); };

                tdBtn.append(odobrenBtn);

                let zabraniBtn = document.createElement('button');
                zabraniBtn.id = x.MyGuid + '_' + 'zabraniBtn';
                zabraniBtn.innerHTML = 'Zabrani';
                zabraniBtn.onclick = () => { zabraniKomentar(x.MyGuid, tdBtn); };

                tdBtn.append(zabraniBtn);
            }
            else {
                tdBtn.innerHTML = 'Banned';
            }
        }
        else {
            tdBtn.innerHTML = 'Approved';
        }

        if (!managerUsing) {
            tdBtn.remove();
        }
    });
}

function createCommentsHeader(tableId) {
    
    let tr = document.createElement('tr');
    let th1 = document.createElement('th');
    let th3 = document.createElement('th');
    let th4 = document.createElement('th');

    $('#' + tableId).append(tr);
    tr.append(th1);
    tr.append(th3);
    tr.append(th4);

    tr.id = 'commentsTableHeader';

    th1.innerHTML = 'Aranzman:';
    th3.innerHTML = 'Turista:';
    th4.innerHTML = 'Komentar:';
}

function addAranzmanComment(aranzmanGuid, table) {

    $.ajax({
        url: 'api/Aranzmani/GetOne',
        type: 'GET',
        contentType: 'application/json',
        dataType: 'json',
        data: $.param({ 'guid': aranzmanGuid }, true),
        success: function (data) {
            if (data) {
                aranzmanBasicTable(data, table);
                aranzmanMoreInfoTable(data, table, false);
            }
        },
        error: function (error) {
            alert(JSON.stringify(error));
        }
    });
}

function addTuristaComment(turGuid, table, managUsing) {
    $.ajax({
        url: 'api/Users/GetOneUser',
        type: 'GET',
        contentType: 'application/json',
        dataType: 'json',
        data: $.param({ 'guid': turGuid }, true),
        success: function (data) {
            if (data) {
                let tr = document.createElement('tr');
                let td = document.createElement('td');
                table.append(tr);
                tr.append(td);

                if (managUsing) {
                    td.innerHTML =
                        'Username:' + data.KorisnickoIme + '<br/>' +
                        'First Name:' + data.Ime + '<br/>' +
                        'Last Name:' + data.Prezime + '<br/>' +
                        'Pol:' + data.Pol + '<br/>' +
                        'Email:' + data.Email;
                }
                else {
                    td.innerHTML =
                        'Username:' + data.KorisnickoIme;
                }

            }
        },
        error: function (error) {
            alert(JSON.stringify(error));
        }
    });
}

function addKomentarComment(comment, table) {

    let tr = document.createElement('tr');
    let td = document.createElement('td');
    table.append(tr);
    tr.append(td);

    td.innerHTML =
        'Tekst:' + comment.Tekst + '<br/>' +
        'Ocena:' + comment.Ocena + '<br/>' +
        (comment.Odobren ? 'Odobren' : 'Nije odobren');
}

function odobriKomentar(komGuid, tdBtn) {

    $.ajax({
        url: 'api/Komentar/ApproveBanComment',
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify({
            guid: komGuid,
            odobren: true,
            zabranjen: false
        }),
        success: function (data) {
            $('#' + komGuid + '_' + 'odobrenBtn').remove();
            $('#' + komGuid + '_' + 'zabraniBtn').remove();
            $(tdBtn).html('Approved');
        },
        error: function (error) {
            alert(JSON.stringify(error));
        }
    });
}

function zabraniKomentar(komGuid, tdBtn) {
    $.ajax({
        url: 'api/Komentar/ApproveBanComment',
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify({
            guid: komGuid,
            odobren: false,
            zabranjen: true
        }),
        success: function (data) {
            $('#' + komGuid + '_' + 'odobrenBtn').remove();
            $('#' + komGuid + '_' + 'zabraniBtn').remove();
            $(tdBtn).html('Banned');
        },
        error: function (error) {
            alert(JSON.stringify(error));
        }
    });
}