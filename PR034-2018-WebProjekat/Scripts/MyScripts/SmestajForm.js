﻿
// niz json objekata
var smestajneJedinice = [];
var smestajneJediniceGuids = [];

$(document).ready(function () {

        if ($('#dodajSmetajTable').length != 0)
            return;
            
        let div = document.getElementById('containerSmestaj');

        let table = document.createElement('table');
        table.border = '1px solid black';
        table.id = 'dodajSmetajTable'

        let tr1 = document.createElement('tr');
        let th1 = document.createElement('th');
        th1.innerHTML = 'Novi smestaj:'
        th1.colSpan = 2;
        tr1.append(th1);
        table.append(tr1);

        let tr2 = document.createElement('tr');
        let td2 = document.createElement('td');
        let th2 = document.createElement('th');

        th2.innerHTML = 'Naziv:'

        let inpt2 = document.createElement('input');
        inpt2.type = 'text';
        inpt2.id = 'nazivSmestaja';
        td2.append(inpt2);

        tr2.append(th2);
        tr2.append(td2);

        table.append(tr2);

        let sel1 = document.createElement('select');
        sel1.id = 'tipSmestaja';
        let opt11 = document.createElement('option');
        opt11.innerHTML = 'Hotel';
        opt11.value = 0;
        let opt12 = document.createElement('option');
        opt12.innerHTML = 'Motel';
        opt12.value = 1;
        let opt13 = document.createElement('option');
        opt13.innerHTML = 'Vila';
        opt13.value = 2;

        sel1.append(opt12);
        sel1.append(opt11);
        sel1.append(opt13);

        $(sel1).change(function () {

            if (sel1.value != 0) {
                $('#zvezdiceRow').remove();
                return;
            }
            else if ($('#zvezdiceRow').length > 0)
                return;

            let tr16 = document.createElement('tr');
            let td16 = document.createElement('td');
            let th16 = document.createElement('th');

            th16.innerHTML = 'Broj Zvezdica';

            let sel6 = document.createElement('select');
            sel6.id = 'brojZvezdica';
            let opt116 = document.createElement('option');
            opt116.innerHTML = '1';
            opt116.value = 1;
            let opt126 = document.createElement('option');
            opt126.innerHTML = '2';
            opt126.value = 2;
            let opt136 = document.createElement('option');
            opt136.innerHTML = '3';
            opt136.value = 3;
            let opt146 = document.createElement('option');
            opt146.innerHTML = '4';
            opt146.value = 4;
            let opt156 = document.createElement('option');
            opt156.innerHTML = '5';
            opt156.value = 5;

            sel6.append(opt116);
            sel6.append(opt126);
            sel6.append(opt136);
            sel6.append(opt146);
            sel6.append(opt156);

            td16.append(sel6);

            tr16.append(th16);
            tr16.append(td16);
            tr16.id = 'zvezdiceRow';

            $(tr16).insertAfter($('#selectRow'));
        });

        let tr3 = document.createElement('tr');
        let td3 = document.createElement('td');
        let th3 = document.createElement('th');

        th3.innerHTML = 'Tip:';
        td3.append(sel1);

        tr3.append(th3);
        tr3.append(td3);
        tr3.id = 'selectRow';

        table.append(tr3);

        
        let tr4 = document.createElement('tr');
        let td4 = document.createElement('td');
        let th4 = document.createElement('th');
        th4.innerHTML = 'Dostupne funkcionalnosti:';
        
        let fs = document.createElement('fieldset');
        let inpt41 = document.createElement('input');
        inpt41.name = 'funkcionalnosti1';
        inpt41.type = 'checkbox';
        inpt41.id = 'bazen';
        inpt41.value = 'bazen';
        
        let inpt42 = document.createElement('input');
        inpt42.name = 'funkcionalnosti2';
        inpt42.type = 'checkbox';
        inpt42.id = 'spaCentar';
        inpt42.value = 'spaCentar';

        let inpt43 = document.createElement('input');
        inpt43.name = 'funkcionalnosti3';
        inpt43.type = 'checkbox';
        inpt43.id = 'wifi';
        inpt43.value = 'wifi';

        let inpt44 = document.createElement('input');
        inpt44.name = 'funkcionalnosti4';
        inpt44.type = 'checkbox';
        inpt44.id = 'invaliditet';
        inpt44.value = 'invaliditet';

        fs.append(inpt41);
        fs.append(inpt42);
        fs.append(inpt43);
        fs.append(inpt44);

        let l41 = document.createElement('label');
        l41.setAttribute('for', 'bazen');
        l41.innerHTML = 'Bazen';
        let l42 = document.createElement('label');
        l42.setAttribute('for', 'spaCentar');
        l42.innerHTML = 'Spa Centar';

        let l43 = document.createElement('label');
        l43.setAttribute('for', 'wifi');
        l43.innerHTML = 'WiFi';

        let l44 = document.createElement('label');
        l44.setAttribute('for', 'invaliditet');
        l44.innerHTML = 'Osobe sa invaliditetom';

        $(l41).insertAfter(inpt41);
        $(l42).insertAfter(inpt42);
        $(l43).insertAfter(inpt43);
        $(l44).insertAfter(inpt44);

        $('<br/>').insertAfter(l41);
        $('<br/>').insertAfter(l42);
        $('<br/>').insertAfter(l43);
        $('<br/>').insertAfter(l44);

        td4.append(fs);
          
        tr4.append(th4);
        tr4.append(td4);

        table.append(tr4);

        let tr5 = document.createElement('tr');
        let td5 = document.createElement('td');

        let inpt55 = document.createElement('button');

        $(inpt55).click(function () {
            if ($('#addSjTable').length > 0)
                return;

            let tbl2 = document.createElement('table');
            tbl2.id = 'addSjTable';
            let th1 = document.createElement('th');
            let td1 = document.createElement('td');
            let tr1 = document.createElement('tr');

            th1.innerHTML = 'Broj gostiju:';
            let brGost = document.createElement('input');
            brGost.type = 'number';
            brGost.validationMessage = 'Mora biti pozitivan broj';
            brGost.id = 'brojGostijuSj';
            td1.append(brGost);
            tr1.append(th1);
            tr1.append(td1);

            tbl2.append(tr1);

            let th2 = document.createElement('th');
            let td2 = document.createElement('td');
            let tr2 = document.createElement('tr');

            th2.innerHTML = 'Cena:';
            let cenaSj = document.createElement('input');
            cenaSj.type = 'number';
            cenaSj.validationMessage = 'Mora biti pozitivan broj';
            cenaSj.id = 'cenaSj';
            td2.append(cenaSj);
            tr2.append(th2);
            tr2.append(td2);

            tbl2.append(tr2);

            let th3 = document.createElement('th');
            let td3 = document.createElement('td');
            let tr3 = document.createElement('tr');

            th3.innerHTML = 'Pet friendly:';
            tr3.append(th3);

            let petFrndly = document.createElement('select');
            petFrndly.id = 'petFriendly';
            let opt1 = document.createElement('option');
            opt1.value = 1;
            opt1.text = 'Da';
            let opt2 = document.createElement('option');
            opt2.value = 0;
            opt2.text = 'Ne';
            petFrndly.append(opt1);
            petFrndly.append(opt2);
            td3.append(petFrndly);
            tr3.append(td3);

            tbl2.append(tr3);

            let td4 = document.createElement('td');
            let tr4 = document.createElement('tr');

            let sacuvajSj = document.createElement('button');
            sacuvajSj.innerText = 'Sacuvaj';
            sacuvajSj.id = 'sacuvajSj';
            td4.colSpan = 2;
            td4.align = 'center';
            td4.append(sacuvajSj);
            tr4.append(td4);
            tbl2.append(tr4);

            $('#containerSmestajnaJed').append(tbl2);

            $(sacuvajSj).click(function () {
                addSmestajnaJedinica();
            });
        });

        inpt55.id = 'dodajSmestajnuJedinicu';
        inpt55.value = 'Dodaj Smestajnu Jedinicu';
        inpt55.innerText = 'Dodaj Smestajnu Jedinicu';
        td5.colSpan = 2;
        td5.align = 'center';

        td5.append(inpt55);
        tr5.append(td5);
        
        table.append(tr5);


        let tr6 = document.createElement('tr');
        let td6 = document.createElement('td');

        let inpt66 = document.createElement('button');
        $(inpt66).click(addSmestaj);

        inpt66.id = 'sacuvajSmestaj';

        inpt66.value = 'Sacuvaj Smestaj';
        inpt66.innerText = 'Sacuvaj Smestaj';
        td6.colSpan = 2;
        td6.align = 'center';

        td6.append(inpt66);
        tr6.append(td6);
        
        table.append(tr6);

        div.append(table);

        // fill existing smestaj
        fillSmestaj();
});

function fillSmestaj()
{
    if (window.localStorage.getItem('smestajToChange') &&
        document.referrer.includes('SviSmestajevi.html'))
    {
        let jsonSmestaj = JSON.parse(window.localStorage.getItem('smestajToChange'));


        // load smestaj
        $('#nazivSmestaja').val(jsonSmestaj.Naziv);
        $('#tipSmestaja').val(jsonSmestaj.TipSmestaja);
        $('#bazen').prop('checked', jsonSmestaj.Bazen);
        $('#spaCentar').prop('checked', jsonSmestaj.SpaCentar);
        $('#wifi').prop('checked', jsonSmestaj.Wifi);
        $('#invaliditet').prop('checked', jsonSmestaj.Invaliditet);

        if (jsonSmestaj.TipSmestaja == 0) {
            // show stars
            $('#tipSmestaja').change();
            $('#brojZvezdica').val(jsonSmestaj.Zvezdice);
        }

        //smestajneJedinice = jsonSmestaj.SmestajneJedinice;
        smestajneJediniceGuids = jsonSmestaj.SmestajneJedinice;

        (new Promise((resolve, reject) => {
            $.ajax({
                url: 'api/SmestajneJedinice/GetMultiple',
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                data: $.param({ 'guids': smestajneJediniceGuids }, true),
                success: function (data) {
                    resolve(data);
                },
                error: function (data) {
                    reject(data);
                }
            })
        })).then((data) => {
            data.forEach((x) => nizSj(x));
        })
        .catch((error) => {
            alert(JSON.stringify(error));
        })
    }
}

function addSmestajnaJedinica()
{
    if (!validateSj()) return;

    var jsonSj = {
        BrojGostiju: Number($('#brojGostijuSj').val()),
        PetFriendly: Number($('#petFriendly').val()),
        Cena: Number($('#cenaSj').val()),
        Rezervisana: 0,
        Obrisana: 0
    };

    nizSj(jsonSj);

    $('#brojGostijuSj').val('');
    $('#petFriendly').val(1);
    $('#cenaSj').val('');
}

function validateSj()
{
    if (Number($('#brojGostijuSj').val()) <= 0) {
        $('#brojGostijuSj').val('');
        $('#brojGostijuSj').css('border-color', 'red');
        return false;
    }
    else
    {
        $('#brojGostijuSj').css('border-color', '');
    }

    if (Number($('#cenaSj').val()) <= 0) {
        $('#cenaSj').val('');
        $('#cenaSj').css('border-color', 'red');
        return false;
    }
    else
    {
        $('#cenaSj').css('border-color', '');
    }

    return true;
}

function nizSj(jsonSj)
{
    if ($('#tableNizSj').length == 0)
    {
        let tblSj = document.createElement('table');
        tblSj.id = 'tableNizSj'; 
        $('#containerNizSj').append(tblSj);

        let tr = document.createElement('tr');
        let th1 = document.createElement('th');
        th1.innerHTML = 'Broj gostiju:';
        let th2 = document.createElement('th');
        th2.innerHTML = 'Cena:';
        let th3 = document.createElement('th');
        th3.innerHTML = 'Pet Friendly:';
        let th4 = document.createElement('th');
        th4.innerHTML = 'Rezervisana:';
        
        tr.append(th1);
        tr.append(th2);
        tr.append(th3);
        tr.append(th4);

        tblSj.append(tr);
    }

    let tr = document.createElement('tr');
    tr.id = smestajneJedinice.length - 1;

    let td1 = document.createElement('td');
    let td2 = document.createElement('td');
    let td3 = document.createElement('td');
    let td4 = document.createElement('td');
    let td5 = document.createElement('td');

    let inpt1 = document.createElement('input');
    inpt1.type = 'number';
    inpt1.value = jsonSj.BrojGostiju;

    let inpt2 = document.createElement('input');
    inpt2.type = 'number';
    inpt2.value = jsonSj.Cena;

    let inpt3 = document.createElement('input');
    inpt3.type = 'checkbox';
    inpt3.value = jsonSj.PetFriendly == true ? 1 : 0;
    inpt3.checked = jsonSj.PetFriendly == true ? 1 : 0;

    let inpt4 = document.createElement('input');
    inpt4.type = 'checkbox';
    inpt4.value = jsonSj.Rezervisana == true ? 1 : 0;
    inpt4.checked = jsonSj.Rezervisana == true ? 1 : 0;

    let inpt5 = document.createElement('button');
    inpt5.innerText = 'Delete';
    inpt5.onclick = function () {
        tr.remove();
        jsonSj.Obrisana = 1;
    };
    
    td1.append(inpt1);
    td2.append(inpt2);
    td3.append(inpt3);
    td4.append(inpt4);
    td5.append(inpt5);

    tr.append(td1);
    tr.append(td2);
    tr.append(td3);
    tr.append(td4);
    tr.append(td5);

    $('#tableNizSj').append(tr);
    // add to table

    smestajneJedinice.push(jsonSj);
}

function validateSmestaj()
{
    if ($('#nazivSmestaja').val().length < 1) {
        $('#nazivSmestaja').val('');
        $('#nazivSmestaja').css('border-color', 'red');
        return false;
    }
    else
    {
        $('#nazivSmestaja').css('border-color', '');
        return true;
    }
}

var sacuvaniSmestaj;

function saveSmestajneJedinice(smestajneJediniceJson)
{
    return new Promise(function (resolve, reject) {
        $.ajax({
            async: false,
            url: 'api/SmestajneJedinice',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(smestajneJediniceJson),
            success: function (result) {
                resolve(result);
            },
            error: function (result) {
                alert(result.responseText)
                reject(result);
            }
        });
    }); 
}

function addSmestaj()
{
    if (!validateSmestaj()) return;
    if (!validateTableSj()) return;

    saveSmestajneJedinice(smestajneJedinice).then(function (result) {
        smestajneJediniceGuids = result;

        let jsonSmestaj = {
            TipSmestaja: $('#tipSmestaja').val(),
            Naziv: $('#nazivSmestaja').val(),
            Bazen: $('#bazen').prop('checked'),
            SpaCentar: $('#spaCentar').prop('checked'),
            Invaliditet: $('#invaliditet').prop('checked'),
            Wifi: $('#wifi').prop('checked'),
            SmestajneJedinice: smestajneJediniceGuids,
            Obrisan: 0
        };

        let smestajToChangeTemp = window.localStorage.getItem('smestajToChange');
        let smestajToChangeGuid = "";

        if (smestajToChangeTemp) {
            jsonSmestaj.MyGuid = JSON.parse(smestajToChangeTemp).MyGuid;
        }
        
        if ($('#tipSmestaja').val() == 0) {
            jsonSmestaj.Zvezdice = $('#brojZvezdica').val();
        }

        sacuvaniSmestaj = jsonSmestaj;

        // ako se dodaje u okviru aranzmana
        //izabraniSmestaj = sacuvaniSmestaj.MyGuid;
        guidForSmestaj = sacuvaniSmestaj.MyGuid;

        $('#dodajSmestaj').parent().html($('#nazivSmestaja').val());

        $('#floating-panel').children().each(function () {
            this.remove();
        });

        $('#dodajSmestaj').remove();
        //

        $.ajax({
            url: 'api/Smestaj',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            success: function (result) {
                alert('Dodat smestaj!');
                izabraniSmestaj = result;
                guidForSmestaj = result;
                console.log(result);
                //location.reload();
            },
            error: function (result) {
                var json = JSON.parse(result.responseText);
                alert(json['Message']);
            },
            data: JSON.stringify(jsonSmestaj)
        });
    })
    .catch(function (result) {
        alert(result);
    });
}

function validateTableSj() {
    let sjRows = $('#tableNizSj').children('tr');

    // starts from 1 because of the header row
    for (let i = 1; i < sjRows.length; i++) {
        let tds = sjRows[i].childNodes;

        smestajneJedinice[i - 1].BrojGostiju = tds[0].firstChild.value;
        smestajneJedinice[i - 1].Cena = tds[1].firstChild.value;
        smestajneJedinice[i - 1].PetFriendly = Number(tds[2].firstChild.value);
        smestajneJedinice[i - 1].Rezervisana = Number(tds[3].firstChild.value);
    }

    return true;
}