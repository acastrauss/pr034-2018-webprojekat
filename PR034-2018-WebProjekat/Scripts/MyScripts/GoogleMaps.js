﻿var currMarker = [];

function initMap() {
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 8,
        //beograd 
        center: { lat: 44.792, lng: 20.469 },
    });
  
    const geocoder = new google.maps.Geocoder();
    const infowindow = new google.maps.InfoWindow();
  
    map.addListener("click", (e) => {
        //placeMarkerAndPanTo(e.latLng, map);
        geocodeLatLng(geocoder, map, infowindow, e.latLng);
    });
}

function geocodeLatLng(geocoder, map, infowindow, latlngP) {
    const input = new String(latlngP);
    const latlngStr = input.split(",", 2);
    const latlng = {
    lat: parseFloat(latlngStr[0].slice(1)),
    lng: parseFloat(latlngStr[1]),
    };
    geocoder
    .geocode({ location: latlng })
    .then((response) => {
        if (response.results[0]) {
        map.setZoom(11);
        const marker = new google.maps.Marker({
            position: latlng,
            map: map,
        });
        map.panTo(latlng);
      
        currMarker.push(marker);

        if (currMarker.length == 2)
        {
            currMarker[0].setMap(null);
            currMarker.shift();
        }
            
        $('#latlng').val(marker.getPosition().lat() + ',' + marker.getPosition().lng());

        infowindow.setContent(response.results[0].formatted_address);
        infowindow.open(map, marker);
        $('#adressText').val(response.results[0].formatted_address);  
        
        } else {
        window.alert("No results found");
        }
    })
    .catch((e) => window.alert("Geocoder failed due to: " + e));
}

function placeMarkerAndPanTo(latLng, map) {
    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
    });
    map.panTo(latLng);

    currMarker.forEach((mark) =>
    {
        mark.setMap(null)
    });

    currMarker.push(marker);

    $('#latlng').val(marker.getPosition().lat() + ',' + marker.getPosition().lng());
}
