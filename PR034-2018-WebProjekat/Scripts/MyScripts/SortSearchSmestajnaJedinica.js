﻿
var smestajeviSjToShow = [];

$(document).ready(function () {

    $('#sortBtnSJ').click(function () {

        let smestajGuids = [];
        sviSmestaji.forEach(s => smestajGuids.push(s.MyGuid));

        promiseSmestajneJediniceZaViseSmestaja(smestajGuids)
            .then((data) => {

                data = JSON.parse(data);
                $('#allAccomTable').html('');
                smestajeviSjToShow = [...sviSmestaji];
                let byWhat = new Number($('#sortByWhatSJ').val());
                let how = new Number($('#sortHowSJ').val());

                if (byWhat == 0) {
                    // po broj gostiju
                    for (let i = 0; i < smestajeviSjToShow.length; i++) {
                        // preuzme json objekte smestajnih jedinica
                        let sjReserve = [...data[smestajeviSjToShow[i].MyGuid]];

                        sjReserve.sort((a, b) => {
                            if (how == 0) {
                                if (a.BrojGostiju < b.BrojGostiju) return -1;
                                if (a.BrojGostiju > b.BrojGostiju) return 1;
                                return 0;
                            }
                            else {
                                if (a.BrojGostiju < b.BrojGostiju) return 1;
                                if (a.BrojGostiju > b.BrojGostiju) return -1;
                                return 0;
                            }
                        });

                        // put them in order so method for showing would show them correctly
                        smestajeviSjToShow[i].SmestajneJedinice = [];
                        sjReserve.forEach(x => smestajeviSjToShow[i].SmestajneJedinice.push(x.MyGuid));
                    }
                }
                else if (byWhat == 1) {
                    for (let i = 0; i < smestajeviSjToShow.length; i++) {

                        let sjReserve = [...data[smestajeviSjToShow[i].MyGuid]];
                        sjReserve.sort((a, b) => {
                            if (how == 0) {
                                if (a.Cena < b.Cena) return -1;
                                if (a.Cena > b.Cena) return 1;
                                return 0;
                            }
                            else {
                                if (a.Cena < b.Cena) return 1;
                                if (a.Cena > b.Cena) return -1;
                                return 0;
                            }
                        });

                        // put them in order so method for showing would show them correctly
                        smestajeviSjToShow[i].SmestajneJedinice = [];
                        sjReserve.forEach(x => smestajeviSjToShow[i].SmestajneJedinice.push(x.MyGuid));
                    }
                }

                showSmestajManagement(smestajeviSjToShow);

            });
    });

    $('#doSearchSJ').click(function () {

        console.log('Nakon klika:' + JSON.stringify(sviSmestaji));

        $('#allAccomTable').html('');

        smestajeviSjToShow = [...sviSmestaji];

        let smestajGuids = [];
        sviSmestaji.forEach(s => smestajGuids.push(s.MyGuid));
        
        promiseSmestajneJediniceZaViseSmestaja(smestajGuids)
            .then((data) => {

                console.log('Nakon promisea:' + JSON.stringify(sviSmestaji));

                data = JSON.parse(data);

                let minGuests = new Number($('#minValueGuests').val());
                let maxGuests = new Number($('#maxValueGuests').val());
                let petFriendly = $('#petFriendly').is(':checked');
                let cena = new Number($('#cenaSJ').val());

                if (minGuests != 0) {
                    console.log('min');

                    for (let i = 0; i < smestajeviSjToShow.length; i++) {
                        // json objekat smestajnih jedinica
                        let sjForSmestaj = [...data[smestajeviSjToShow[i].MyGuid]];

                        let indxToRemove = [];

                        for (let j = 0; j < sjForSmestaj.length; j++) {
                            if (sjForSmestaj[j].BrojGostiju < minGuests)
                                indxToRemove.push(j);
                        }

                        indxToRemove = [...new Set(indxToRemove)];

                        for (let j = 0; j < indxToRemove.length; j++) {
                            smestajeviSjToShow[i].SmestajneJedinice.splice(j, 1);
                            for (let k = j + 1; k < indxToRemove.length; k++) {
                                indxToRemove[k]--;
                            }
                        }
                    }
                }

                if (maxGuests != 0) {
                    console.log('max');

                    for (let i = 0; i < smestajeviSjToShow.length; i++) {
                        // json objekat smestajnih jedinica
                        let sjForSmestaj = [...data[smestajeviSjToShow[i].MyGuid]];

                        let indxToRemove = [];

                        for (let j = 0; j < sjForSmestaj.length; j++) {
                            if (sjForSmestaj[j].BrojGostiju > maxGuests)
                                indxToRemove.push(j);
                        }

                        indxToRemove = [...new Set(indxToRemove)];

                        for (let j = 0; j < indxToRemove.length; j++) {
                            smestajeviSjToShow[i].SmestajneJedinice.splice(j, 1);
                            for (let k = j + 1; k < indxToRemove.length; k++) {
                                indxToRemove[k]--;
                            }
                        }
                    }
                }

                if (petFriendly) {
                    console.log('pf');

                    for (let i = 0; i < smestajeviSjToShow.length; i++) {
                        // json objekat smestajnih jedinica
                        let sjForSmestaj = [...data[smestajeviSjToShow[i].MyGuid]];

                        let indxToRemove = [];

                        for (let j = 0; j < sjForSmestaj.length; j++) {
                            if (!sjForSmestaj[j].PetFriendly)
                                indxToRemove.push(j);
                        }

                        indxToRemove = [...new Set(indxToRemove)];

                        for (let j = 0; j < indxToRemove.length; j++) {
                            smestajeviSjToShow[i].SmestajneJedinice.splice(j, 1);
                            for (let k = j + 1; k < indxToRemove.length; k++) {
                                indxToRemove[k]--;
                            }
                        }
                    }
                }
                else {
                    console.log('nepf');

                    for (let i = 0; i < smestajeviSjToShow.length; i++) {
                        // json objekat smestajnih jedinica
                        let sjForSmestaj = [...data[smestajeviSjToShow[i].MyGuid]];

                        let indxToRemove = [];

                        for (let j = 0; j < sjForSmestaj.length; j++) {
                            if (sjForSmestaj[j].PetFriendly)
                                indxToRemove.push(j);
                        }

                        indxToRemove = [...new Set(indxToRemove)];

                        for (let j = 0; j < indxToRemove.length; j++) {
                            smestajeviSjToShow[i].SmestajneJedinice.splice(j, 1);
                            for (let k = j + 1; k < indxToRemove.length; k++) {
                                indxToRemove[k]--;
                            }
                        }
                    }
                }

                if (cena != 0) {
                    console.log('cena');

                    console.log(smestajeviSjToShow);
                    for (let i = 0; i < smestajeviSjToShow.length; i++) {
                        // json objekat smestajnih jedinica
                        let sjForSmestaj = [...data[smestajeviSjToShow[i].MyGuid]];

                        let indxToRemove = [];

                        for (let j = 0; j < sjForSmestaj.length; j++) {
                            if (sjForSmestaj[j].Cena != cena) {
                                console.log(`${sjForSmestaj[j].Cena} != ${cena}`);

                                indxToRemove.push(j);

                            }
                            else
                                console.log(`${sjForSmestaj[j].Cena} == ${cena}`);

                        }

                        indxToRemove = [...new Set(indxToRemove)];

                        for (let j = 0; j < indxToRemove.length; j++) {
                            smestajeviSjToShow[i].SmestajneJedinice.splice(j, 1);
                            for (let k = j + 1; k < indxToRemove.length; k++) {
                                indxToRemove[k]--;
                            }
                        }
                    }
                }
                console.log(smestajeviSjToShow);
                showSmestajManagement(smestajeviSjToShow);
            });
    });

    $('#cancelSearchSJ').click(function () {
        $('#allAccomTable').html('');

        $('#minValueGuests').val('');
        $('#maxValueGuests').val('');
        $('#petFriendly').prop('checked', false);
        $('#cenaSJ').val('');


        $.ajax({
            url: 'api/Smestaj',
            type: 'GET',
            success: function (result) {
                sviSmestaji = [...result];

                for (let i = 0; i < sviSmestaji.length; i++) {
                    sviSmestaji[i] = JSON.parse(sviSmestaji[i]);
                }

                smestajeviSjToShow = [...sviSmestaji];

                showSmestajManagement(smestajeviSjToShow);
            }
        });
    });
});

function promiseSmestajneJediniceZaViseSmestaja(smestajGuids)
{
    return (new Promise((resolve, reject) => {
        $.ajax({
            url: 'api/SmestajneJedinice/GetSmestajneJediniceForMultipleSmestaj',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            data: $.param({ 'smestajGuids': smestajGuids }, true),
            success: function (result) {
                resolve(result);
            },
            error: function (result) {
                alert(result.responseText);
            }
        })
    }));
}