﻿var sviSmestaji = [];


$(document).ready(function () {

    $('#searchSortSmestaj').load('SortSearchSmestaj.html');
    $('#searchSortSmestajneJedinice').load('SortSearchSmestajnaJedinica.html');

    $.ajax({
        url: 'api/Smestaj',
        type: 'GET',
        success: function (result) {
            sviSmestaji = [...result];

            for (let i = 0; i < sviSmestaji.length; i++) {
                sviSmestaji[i] = JSON.parse(sviSmestaji[i]);
            }

            if (window.sessionStorage.getItem('role') == 'Menadzer')
                showSmestajManagement(sviSmestaji, true);
            else
                showSmestajManagement(sviSmestaji, false);

            console.log('Ucitano:' + JSON.stringify(sviSmestaji)); 
        },
        error: function (res)
        {
            alert(res.responseText);
            window.location.href = '/Index.html';
        }
    });

});


function showSmestajManagement(smestajevi, managerUsing = false)
{
    let tr = document.createElement('tr');
    $('#allAccomTable').append(tr);

    for (let i = 0; i < smestajevi.length; i++) {
        let jsonSmestaj = smestajevi[i];
        let tbl = document.createElement('table');
        let td = document.createElement('td');
        tr.append(td);
        td.append(tbl);

        smestajTable(jsonSmestaj, tbl, "", true);

        $(tbl).hover(function () {
            if (!managerUsing) return;

            let changeBtn = document.createElement('button');
            changeBtn.id = "change_" + jsonSmestaj.MyGuid;
            changeBtn.innerText = 'Change';
            changeBtn.onclick = function () {
                window.localStorage.setItem('smestajToChange', JSON.stringify(jsonSmestaj));
                window.location.href = '/SmestajForm.html';
            };

            let deleteBtn = document.createElement('button');
            deleteBtn.id = "delete_" + jsonSmestaj.MyGuid;
            deleteBtn.innerText = 'Delete';
            deleteBtn.onclick = function () {
                $.ajax({
                    url: 'api/Smestaj',
                    type: 'DELETE',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify(
                        {
                            smestajGuid: jsonSmestaj.MyGuid
                        }
                    ),
                    success: function (result) {
                        td.remove();
                        alert('Deleted');
                    },
                    error: function (result) {
                        alert(result.responseText);
                        location.reload();
                    }
                });
            };

            let trChng = document.createElement('tr');
            let tdChng = document.createElement('td');
            trChng.id = "deleteRow_" + jsonSmestaj.MyGuid;
            trChng.append(tdChng);
            tdChng.append(changeBtn);
            tbl.append(trChng);

            let trDel = document.createElement('tr');
            let tdDel = document.createElement('td');
            trDel.id = "deleteRow_" + jsonSmestaj.MyGuid;
            trDel.append(tdDel);
            tdDel.append(deleteBtn);
            tbl.append(trDel);
        },
            function () {
                if (!managerUsing) return;
                $('#deleteRow_' + jsonSmestaj.MyGuid).remove();
                $('#deleteRow_' + jsonSmestaj.MyGuid).remove();
            }
        );
    }

    console.log('Nakon prikazivanja:' + JSON.stringify(sviSmestaji));
}