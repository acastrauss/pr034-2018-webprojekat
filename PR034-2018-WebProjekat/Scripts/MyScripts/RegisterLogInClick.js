﻿
function validateReg()
{
    var re = /^[^\s@]+@[^\s@]+$/;

    if ($('#Username').val().length < 1) {
        $('#Username').css('border', '1px solid red');
        return false;
    }
    else
    {
        $('#Username').css('border', '');
    }

    if ($('#Password').val().length < 1)
    {
        $('#Password').css('border', '1px solid red');
        return false;
    }
    else
    {
        $('#Password').css('border', '');
    }

    if ($('#Name').val().length < 1)
    {
        $('#Name').css('border', '1px solid red');
        return false;
    }
    else
    {
        $('#Name').css('border', '');
    }

    if ($('#Surname').val().length < 1)
    {
        $('#Surname').css('border', '1px solid red');
        return false;
    }
    else
    {
        $('#Surname').css('border', '');
    }

    if (!re.test($('#Email').val()))
    {
        $('#Email').css('border', '1px solid red');
        return false;
    }
    else
    {
        $('#Email').css('border', '');
    }

    if ($('#BirthDate').val() == '')
    {
        $('#BirthDate').css('border', '1px solid red');
        return false;
    }
    else
    {
        $('#BirthDate').css('border', '');
    }
    return true;
}

function validateLog()
{
    if ($('#Username').val().length < 1) {
        $('#Username').css('border', '1px solid red');
        return false;
    }
    else {
        $('#Username').css('border', '');
    }

    if ($('#Password').val().length < 1) {
        $('#Password').css('border', '1px solid red');
        return false;
    }
    else {
        $('#Password').css('border', '');
    }

    return true;
}

$(document).ready(function () {
    $(document).on('click', '#registerBtn', function () {

        if (!validateReg()) return;

        var jsonObj = {
            KorisnickoIme: $('#Username').val(),
            Lozinka: $('#Password').val(),
            Ime: $('#Name').val(),
            Prezime: $('#Surname').val(),
            Pol: $("#Gender").val(),
            Email: $('#Email').val(),
            DatumRodjenja: $('#BirthDate').val(),
            UlogaKorisnika: 2
        };

        $.ajax({
            url: 'api/Users',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            success: function (result) {
                window.sessionStorage.setItem('cookie', result['cookie']);
                window.sessionStorage.setItem('username', result['username']);
                window.sessionStorage.setItem('role', uloga_korisnika[result['role']]);
                location.reload();
            },
            error: function (result) {
                var json = JSON.parse(result.responseText);
                alert(json['Message']);
                $('#formDiv').find('input').each(function () {
                    if(this.type != 'submit')
                        this.value = '';
                });
            },
            data: JSON.stringify(jsonObj)
        });
    });

    $(document).on('click', '#logBtn', function () {
        // log in
        if (!validateLog()) return;

        var jsonObj = {
            KorisnickoIme: $('#Username').val(),
            Lozinka: $('#Password').val(),
        };

        $.ajax({
            url: 'api/Users',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            success: function (result) {
                window.sessionStorage.setItem('cookie', result['cookie']);
                window.sessionStorage.setItem('username', result['username']);
                window.sessionStorage.setItem('role', uloga_korisnika[result['role']]);
                location.reload();
            },
            error: function (result) {
                var json = JSON.parse(result.responseText);
                alert(json['Message']);
                $('#formDiv').find('input').each(function () {
                    if (this.type != 'submit')
                        this.value = '';
                });
            },
            data: JSON.stringify(jsonObj)
        });

    });

    $(document).on('click', '#logOutBtn', function () {
        window.sessionStorage.removeItem('cookie');
        window.sessionStorage.removeItem('username');
        window.sessionStorage.removeItem('role');

        // if manager was signed
        window.sessionStorage.removeItem('myArrangements');

        window.location.href = '/Index.html';
        //location.reload();
    });

    $(document).on('click', '#profileBtn', function () {
        window.location.href = '/Profile.html';
    });

    $(document).on('click', '#homePageBtn', function () {
        window.location.href = '/Index.html';
    });
});