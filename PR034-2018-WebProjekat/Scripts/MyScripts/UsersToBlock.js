﻿$(document).ready(function () {

    $.ajax({
        url: 'api/Users/GetPotentialToBlockUsers',
        type: 'GET',
        contentType: 'application/json',
        dataType: 'json',
        data: $.param({ 'guid': window.sessionStorage.getItem('cookie') }, true),
        success: function (data) {
            addUserToTable(data);
        },
        error: function (error) {
            alert(JSON.stringify(error));
        }
    });
});

function addUserToTable(users)
{
    for (let i = 0; i < users.length; i++) {
        let user = users[i];

        let tr = document.createElement('tr');
        let td1 = document.createElement('td');
        let td2 = document.createElement('td');

        $('#allUsersTable').append(tr);
        tr.append(td1);
        tr.append(td2);

        // button setup
        let divBlock = document.createElement('div');
        divBlock.id = 'divBlock_' + user.MyGuid;
        let btnBlock = document.createElement('button');
        btnBlock.id = 'block_' + user.MyGuid;
        btnBlock.innerHTML = 'Block';

        divBlock.append(btnBlock);
        td2.append(divBlock);

        btnBlock.onclick = () =>
        {
            blockUser(user.MyGuid);
        };
        //

        // table setup
        let userTable = document.createElement('table');

        let tr1 = document.createElement('tr');
        let tr2 = document.createElement('tr');
        let tr3 = document.createElement('tr');
        let tr4 = document.createElement('tr');
        let tr5 = document.createElement('tr');
        let tr6 = document.createElement('tr');

        td1.append(userTable);
        userTable.append(tr1);
        userTable.append(tr2);
        userTable.append(tr3);
        userTable.append(tr4);
        userTable.append(tr5);
        userTable.append(tr6);

        let th1 = document.createElement('th');
        let td11 = document.createElement('td');
        th1.innerHTML = 'Username:';
        td11.innerHTML = user.KorisnickoIme;
        tr1.append(th1);
        tr1.append(td11);

        let th2 = document.createElement('th');
        let td12 = document.createElement('td');
        th2.innerHTML = 'Ime:';
        td12.innerHTML = user.Ime;
        tr2.append(th2);
        tr2.append(td12);

        let th3 = document.createElement('th');
        let td13 = document.createElement('td');
        th3.innerHTML = 'Prezime:';
        td13.innerHTML = user.Prezime;
        tr3.append(th3);
        tr3.append(td13);

        let th4 = document.createElement('th');
        let td14 = document.createElement('td');
        th4.innerHTML = 'Datum rodjenja:';
        td14.innerHTML = user.DatumRodjenja;
        tr4.append(th4);
        tr4.append(td14);

        let th5 = document.createElement('th');
        let td15 = document.createElement('td');
        th5.innerHTML = 'Email:';
        td15.innerHTML = user.Email;
        tr5.append(th5);
        tr5.append(td15);

        let th6 = document.createElement('th');
        let td16 = document.createElement('td');
        th6.innerHTML = 'Broj otkazanih:';
        td16.innerHTML = user.BrojOtkazanih;
        tr6.append(th6);
        tr6.append(td16);


        //
    }
}

function blockUser(userGuid)
{
    $.ajax({
        url: 'api/Users/BlockUser',
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify({ guid: userGuid }),
        success: function (data) {
            $('#divBlock_' + userGuid).html('Blocked');
        },
        error: function (error) {
            alert(JSON.stringify(error));
        }
    });
}