﻿$(document).ready(function (e) {
    if (window.sessionStorage.getItem('cookie') == null) {
        var divReg = document.getElementById('regBtns');
        var regBtn = document.createElement('button');
        regBtn.innerHTML = 'Register';
        regBtn.className = 'regButton';
        regBtn.id = 'regBtn';
        divReg.append(regBtn);

        var divider = document.createElement('div');
        divider.className = 'divider';
        divReg.append(divider);

        var logBtn = document.createElement('button');
        logBtn.innerHTML = 'Log In';
        logBtn.className = 'regButton';
        logBtn.id = 'logInBtn';
        divReg.append(logBtn);
    }
    else {
        var divReg = document.getElementById('regBtns');
        var logOBtn = document.createElement('button');
        logOBtn.innerHTML = 'Log Out';
        logOBtn.className = 'regButton';
        logOBtn.id = 'logOutBtn';
        divReg.append(logOBtn);

        var divider = document.createElement('div');
        divider.className = 'divider';
        divReg.append(divider);

        if (!window.location.href.includes('/Profile.html')) {
            var profileBtn = document.createElement('button');
            profileBtn.innerHTML = 'My Profile';
            profileBtn.className = 'regButton';
            profileBtn.id = 'profileBtn';
            divReg.append(profileBtn);
        }
        else if (!window.location.href.includes('/Index.html'))
        {
            var profileBtn = document.createElement('button');
            profileBtn.innerHTML = 'Home page';
            profileBtn.className = 'regButton';
            profileBtn.id = 'homePageBtn';
            divReg.append(profileBtn);
        }
    }
});