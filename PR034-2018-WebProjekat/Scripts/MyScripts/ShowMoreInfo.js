﻿
$(document).ready(function () {

    $(document).on('click', '.moreInfoBtn', function () {
        // only change in table of that arrangment
        var tableAr = $(this).parentsUntil('table').last().parent();
        tableAr.find('.aranzmanMoreInfo').removeAttr('hidden');
        $(this).text('Prikazi manje');
        $(this).attr('class', 'lessInfoBtn');
    });

    $(document).on('click', '.lessInfoBtn', function () {
        var tableAr = $(this).parentsUntil('table').last().parent();
        tableAr.find('.aranzmanMoreInfo').attr('hidden', 'true');
        $(this).text('Prikazi vise');
        $(this).attr('class', 'moreInfoBtn');
    });
});