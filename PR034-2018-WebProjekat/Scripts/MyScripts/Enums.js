﻿// enums from c#
const tip_aranzmana = {
    0: 'Nocenje sa doruckom',
    1: 'Polupansion',
    2: 'Pun pansion',
    3: 'All inclusive',
    4: 'Najam apartmana'
};

const tip_prevoza = {
    0: 'Autobus',
    1: 'Avion',
    2: 'Autobus avion',
    3: 'Individualan',
    4: 'Ostalo'
};

const uloga_korisnika = {
    0: 'Administrator',
    1: 'Menadzer',
    2: 'Turista'
};

const status_rezervacije = {
    0: 'Aktivna',
    1: 'Otkazana'
};

const tip_smestaja = {
    0: 'Hotel',
    1: 'Motel',
    2: 'Vila'
};

var emptyGuid = "00000000-0000-0000-0000-000000000000";
