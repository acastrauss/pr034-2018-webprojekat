﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using PR034_2018_WebProjekat.FileAccess;
using PR034_2018_WebProjekat.Models;

namespace PR034_2018_WebProjekat
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
        //    GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings
        //.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        //    GlobalConfiguration.Configuration.Formatters
        //        .Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // oslobodi sve smestajne jedinice za aranzmane koji su prosli
            var aranzmani = Read.ReadAllAranzmane();

            foreach (var ar in aranzmani)
            {
                // ako je aranzman prosao
                if(ar.DatumZavrsetka < DateTime.Now)
                {
                    var sm = Read.ReadSmestaj(ar.SmestajAr.ToString());

                    if (sm == null) continue;

                    foreach (var sjGuid in sm.SmestajneJedinice)
                    {
                        var sj = Read.ReadSmestajnaJedinica(sjGuid.ToString());
                        
                        if(sj != null)
                        {
                            sj.Rezervisana = false;
                            Update.UpdateSmestajnaJedinica(sj);
                        }
                    }
                }
            }

        }
    }
}
