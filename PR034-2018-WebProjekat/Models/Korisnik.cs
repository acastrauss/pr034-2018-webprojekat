﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace PR034_2018_WebProjekat.Models
{
    public enum ULOGA_KORISNIKA { ADMINISTRATOR, MENADZER, TURISTA};

    public class Korisnik 
    {
        protected Korisnik(string korisnickoIme, string lozinka, string ime, string prezime, char pol,
            string email, DateTime datumRodjenja, ULOGA_KORISNIKA uloga = ULOGA_KORISNIKA.TURISTA)
        {
            KorisnickoIme = korisnickoIme;
            Lozinka = lozinka;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            Email = email;
            DatumRodjenja = datumRodjenja;
            UlogaKorisnika = uloga;
            MyGuid = Guid.NewGuid();
            Cookie = MyGuid.ToString();
        }

        public Korisnik()
        {
            KorisnickoIme = String.Empty;
            Lozinka = String.Empty;
            Ime = String.Empty;
            Prezime = String.Empty;
            Pol = 'M';
            Email = String.Empty;
            DatumRodjenja = DateTime.Now;
            UlogaKorisnika = ULOGA_KORISNIKA.TURISTA;
            MyGuid = Guid.NewGuid();
            Cookie = MyGuid.ToString();
        }

        public Korisnik(Korisnik ref1)
        {
            KorisnickoIme = ref1.KorisnickoIme;
            Lozinka = ref1.Lozinka;
            Ime = ref1.Ime;
            Prezime = ref1.Prezime;
            Pol = ref1.Pol;
            Email = ref1.Email;
            DatumRodjenja = ref1.DatumRodjenja;
            UlogaKorisnika = ref1.UlogaKorisnika;
            Cookie = ref1.Cookie;
            MyGuid = ref1.MyGuid;
        }

        public String KorisnickoIme { get; set; }
        public String Lozinka { get; set; }
        public String Ime { get; set; }
        public String Prezime { get; set; }
        public Char Pol { get; set; }
        public String Email { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public ULOGA_KORISNIKA UlogaKorisnika { get; set; }
        public String Cookie { get; set; }
        public Guid MyGuid { get; set; }

        public override bool Equals(object obj)
        {
            Korisnik k;

            try
            {
                k = (Korisnik)obj;
            }
            catch (Exception e)
            {
                return false;
            }

            return 
                k.KorisnickoIme.Equals(this.KorisnickoIme) && k.UlogaKorisnika.Equals(this.UlogaKorisnika) &&
                MyGuid.Equals(k.MyGuid) && Cookie.Equals(k.Cookie);
        }

        public String GetDateFormated()
        {
            return DatumRodjenja.ToString("dd/MM/yyyy");
        }

        public override string ToString()
        {
            String retVal = String.Empty;
            retVal += KorisnickoIme + "\n";
            retVal += Lozinka + "\n";
            retVal += Ime + "\n";
            retVal += Prezime + "\n";
            retVal += Pol + "\n";
            retVal += Email + "\n";
            retVal += this.GetDateFormated() + "\n";
            retVal += Enum.GetName(typeof(ULOGA_KORISNIKA), UlogaKorisnika);
            
            return retVal;
        }

        public override int GetHashCode()
        {
            var sha = SHA256.Create();

            var bytes = Encoding.ASCII.GetBytes(this.ToString());
            
            var hash = sha.ComputeHash(bytes);
            
            return BitConverter.ToInt32(hash, 0);
        }

        public String GetHashString()
        {
            var sha = SHA256.Create();

            var bytes = Encoding.ASCII.GetBytes(this.ToString());

            var hash = sha.ComputeHash(bytes);

            return BitConverter.ToString(hash, 0, 15);
        }
    }
}