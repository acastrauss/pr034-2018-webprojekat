﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR034_2018_WebProjekat.Models
{
    public class Hotel : Smestaj
    {
        public Hotel(string naziv, bool bazen, bool spaCentar, bool invaliditet, bool wifi, List<Guid> smestajneJedinice, bool obrisan
            , uint zvezdice) : base(TIP_SMESTAJA.HOTEL, naziv, bazen, spaCentar, invaliditet, wifi, smestajneJedinice, obrisan)
        {
            Zvezdice = zvezdice;
        }
        
        public Hotel() : base()
        {
            TipSmestaja = TIP_SMESTAJA.HOTEL;
            Zvezdice = 0;
        }

        public Hotel(Hotel ref1)
        {
            Naziv = ref1.Naziv;
            Bazen = ref1.Bazen;
            SpaCentar = ref1.SpaCentar;
            Invaliditet = ref1.Invaliditet;
            Wifi = ref1.Wifi;
            SmestajneJedinice = ref1.SmestajneJedinice;
            Zvezdice = ref1.Zvezdice;
            Obrisan = ref1.Obrisan;
        }

        public uint Zvezdice { get; set; }

        public override string ToString()
        {
            return base.ToString() + Zvezdice.ToString() + "\n";
        }
    }
}