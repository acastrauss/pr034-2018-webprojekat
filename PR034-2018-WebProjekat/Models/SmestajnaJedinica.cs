﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR034_2018_WebProjekat.Models
{
    public class SmestajnaJedinica
    {
        public SmestajnaJedinica(uint brojGostiju, bool petFriendly, double cena, bool rezervisana, bool obrisana)
        {
            BrojGostiju = brojGostiju;
            PetFriendly = petFriendly;
            Cena = cena;
            Rezervisana = rezervisana;
            Obrisana = obrisana;
            MyGuid = Guid.NewGuid();
        }

        public SmestajnaJedinica()
        {
            BrojGostiju = 0;
            PetFriendly = false;
            Cena = 0;
            Rezervisana = false;
            Obrisana = false;
            MyGuid = Guid.NewGuid();
        }

        public SmestajnaJedinica(SmestajnaJedinica ref1)
        {
            BrojGostiju = ref1.BrojGostiju;
            PetFriendly = ref1.PetFriendly;
            Cena = ref1.Cena;
            Rezervisana = ref1.Rezervisana;
            Obrisana = ref1.Obrisana;
            MyGuid = ref1.MyGuid;
        }

        public uint BrojGostiju { get; set; }
        public bool PetFriendly { get; set; }
        public Double Cena { get; set; }
        public bool Rezervisana { get; set; }
        public bool Obrisana { get; set; }
        public Guid MyGuid { get; set; }

        public override string ToString()
        {
            String retVal = String.Empty;

            retVal += BrojGostiju.ToString() + "\n";
            retVal += (PetFriendly ? "Da" : "Ne") + "\n";
            retVal += Cena.ToString() + "\n";
            retVal += Rezervisana ? "Rezervisana" + "\n" : "Slobodna" + "\n";
            retVal += Obrisana ? "Obrisana" + "\n" : "Nije obrisana" + "\n";

            return retVal;
        }

        public override bool Equals(object obj)
        {
            try
            {
                SmestajnaJedinica smestajnaJedinica = (SmestajnaJedinica)obj;
                return
                    MyGuid.Equals(smestajnaJedinica.MyGuid);
                ;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}