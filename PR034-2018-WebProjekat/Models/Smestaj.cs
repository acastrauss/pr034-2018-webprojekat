﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace PR034_2018_WebProjekat.Models
{
    public enum TIP_SMESTAJA { HOTEL, MOTEL, VILA};
    public class Smestaj
    {
        public Smestaj(TIP_SMESTAJA tipSmestaja, string naziv, bool bazen, bool spaCentar, bool invaliditet, bool wifi, List<Guid> smestajneJedinice, bool obrisan)
        {
            TipSmestaja = tipSmestaja;
            Naziv = naziv;
            Bazen = bazen;
            SpaCentar = spaCentar;
            Invaliditet = invaliditet;
            Wifi = wifi;
            SmestajneJedinice = smestajneJedinice;
            Obrisan = obrisan;
            MyGuid = Guid.NewGuid();
        }

        public Smestaj()
        {
            TipSmestaja = TIP_SMESTAJA.MOTEL;
            Naziv = String.Empty;
            Bazen = false;
            SpaCentar = false;
            Invaliditet = false;
            Wifi = false;
            SmestajneJedinice = new List<Guid>();
            Obrisan = false;
            MyGuid = Guid.NewGuid();
        }

        public Smestaj(Smestaj ref1)
        {
            TipSmestaja = ref1.TipSmestaja;
            Naziv = ref1.Naziv;
            Bazen = ref1.Bazen;
            SpaCentar = ref1.SpaCentar;
            Invaliditet = ref1.Invaliditet;
            Wifi = ref1.Wifi;
            SmestajneJedinice = ref1.SmestajneJedinice;
            Obrisan = ref1.Obrisan;
            MyGuid = ref1.MyGuid;
        }

        public TIP_SMESTAJA TipSmestaja { get; set; }
        public String Naziv { get; set; }
        public bool Bazen { get; set; }
        public bool SpaCentar { get; set; }
        public bool Invaliditet { get; set; }
        public bool Wifi { get; set; }
        public List<Guid> SmestajneJedinice { get; set; }
        public bool Obrisan { get; set; }
        public Guid MyGuid { get; set; }

        public override string ToString()
        {
            String retVal = String.Format(
                "{0}\n{1}\n{2}{3}{4}{5}",
                Enum.GetName(typeof(TIP_SMESTAJA), TipSmestaja), Naziv,
                Bazen ? "Pool\n" : "",
                SpaCentar ? "Spa\n" : "",
                Invaliditet ? "For diabled\n" : "",
                Wifi ? "Wifi\n" : "");

            SmestajneJedinice.ForEach(x => retVal += x.ToString() + "\n");
            return retVal;
        }

        public override bool Equals(object obj)
        {
            try
            {
                Smestaj smestaj = (Smestaj)obj;
                return
                    MyGuid.Equals(smestaj.MyGuid);
            }
            catch (Exception)
            {
                return false;
            }

        }

        public String GetHashString()
        {
            var sha = SHA256.Create();

            var bytes = Encoding.ASCII.GetBytes(this.ToString());

            var hash = sha.ComputeHash(bytes);

            return BitConverter.ToString(hash, 0, 15);
        }
    }
}