﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR034_2018_WebProjekat.Models
{
    public class Menadzer : Korisnik
    {
        // guids of arrangements
        public List<Guid> Aranzmani { get; set; }

        public Menadzer() : base()
        {
            Aranzmani = new List<Guid>();
            base.UlogaKorisnika = ULOGA_KORISNIKA.MENADZER;
        }

        public Menadzer(string korisnickoIme, string lozinka, string ime, string prezime, char pol, string email, DateTime datumRodjenja, List<Guid> aranzmani) 
            : base(korisnickoIme, lozinka, ime, prezime, pol, email, datumRodjenja, ULOGA_KORISNIKA.MENADZER)
        {
            Aranzmani = aranzmani;
        }

        public Menadzer(Menadzer ref1)
        {
            KorisnickoIme = ref1.KorisnickoIme;
            Lozinka = ref1.Lozinka;
            Ime = ref1.Ime;
            Prezime = ref1.Prezime;
            Email = ref1.Email;
            DatumRodjenja = ref1.DatumRodjenja;
            UlogaKorisnika = ref1.UlogaKorisnika;
            Aranzmani = ref1.Aranzmani;
        }
    }
}