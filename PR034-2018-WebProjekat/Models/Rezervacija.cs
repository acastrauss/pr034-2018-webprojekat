﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace PR034_2018_WebProjekat.Models
{
    public enum STATUS_REZERVACIJE { AKTIVNA, OTKAZANA, PROSLA};

    public class Rezervacija
    {
        public Rezervacija(Guid turistaRez, STATUS_REZERVACIJE statusRezervacije, Guid aranzmanRez, Guid smestajnaJedinicaRez, Guid komentarGuid)
        {
            TuristaRez = turistaRez;
            StatusRezervacije = statusRezervacije;
            AranzmanRez = aranzmanRez;
            SmestajnaJedinicaRez = smestajnaJedinicaRez;
            MyGuid = Guid.NewGuid();
            KomentarGuid = komentarGuid;    
        }

        public Rezervacija()
        {
            TuristaRez = Guid.Empty;
            StatusRezervacije = STATUS_REZERVACIJE.OTKAZANA;
            AranzmanRez = Guid.Empty;
            SmestajnaJedinicaRez = Guid.Empty;
            MyGuid = Guid.NewGuid();
            KomentarGuid = Guid.Empty;
        }

        public Rezervacija(Rezervacija ref1)
        {
            TuristaRez = ref1.TuristaRez;
            StatusRezervacije = ref1.StatusRezervacije;
            AranzmanRez = ref1.AranzmanRez;
            SmestajnaJedinicaRez = ref1.SmestajnaJedinicaRez;
            MyGuid = ref1.MyGuid;
            KomentarGuid = ref1.KomentarGuid;
        }

        public Guid TuristaRez { get; set; }

        public STATUS_REZERVACIJE StatusRezervacije { get; set; }

        public Guid AranzmanRez { get; set; }

        public Guid SmestajnaJedinicaRez { get; set; }

        public Guid MyGuid { get; set; }

        public Guid KomentarGuid { get; set; }
        public override string ToString()
        {
            String retVal = String.Empty;

            //retVal += TuristaKorisnickoIme.ToString();
            //retVal += Enum.GetName(typeof(STATUS_REZERVACIJE), StatusRezervacije);
            //retVal += AranzmanRez.ToString();
            //retVal += SmestajnaJedinicaRez.ToString();

            return retVal;
        }

        public String GetID()
        {
            var sha = SHA256.Create();

            var bytes = Encoding.ASCII.GetBytes(this.ToString());

            var hash = sha.ComputeHash(bytes);

            return BitConverter.ToString(hash, 0, 15);
        }

        public override bool Equals(object obj)
        {
            try
            {
                return
                    this.MyGuid.Equals(((Rezervacija)obj).MyGuid);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}