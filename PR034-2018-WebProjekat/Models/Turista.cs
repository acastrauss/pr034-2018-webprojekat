﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR034_2018_WebProjekat.Models
{
    public class Turista : Korisnik
    {
        // guid of every reservation
        public List<Guid> Rezervacije { get; set; }
        public uint BrojOtkazanih { get; set; }
        public bool Blokiran { get; set; }

        public Turista() :base()
        {
            Rezervacije = new List<Guid>();
            base.UlogaKorisnika = ULOGA_KORISNIKA.TURISTA;
            BrojOtkazanih = 0;
            Blokiran = false;
        }

        public Turista(string korisnickoIme, string lozinka, string ime, string prezime, char pol, string email, DateTime datumRodjenja
            , List<Guid> rezervacije, uint brojotkazanih, bool blokiran) 
            : base(korisnickoIme, lozinka, ime, prezime, pol, email, datumRodjenja, ULOGA_KORISNIKA.TURISTA)
        {
            Rezervacije = rezervacije;
            BrojOtkazanih = brojotkazanih;
            Blokiran = blokiran;
        }

        public Turista(Turista ref1)
        {
            KorisnickoIme = ref1.KorisnickoIme;
            Lozinka = ref1.Lozinka;
            Ime = ref1.Ime;
            Prezime = ref1.Prezime;
            Email = ref1.Email;
            DatumRodjenja = ref1.DatumRodjenja;
            UlogaKorisnika = ref1.UlogaKorisnika;
            Rezervacije = ref1.Rezervacije;
            BrojOtkazanih = ref1.BrojOtkazanih;
            Blokiran = ref1.Blokiran;
        }
    }
}