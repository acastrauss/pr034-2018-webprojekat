﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace PR034_2018_WebProjekat.Models
{
    public enum TIP_ARANZMANA
    {
        NOCENJE_SA_DORUCKOM, POLUPANSION, PUN_PANSION, ALL_INCLUSIVE, NAJAM_APARTMANA
    }

    public enum TIP_PREVOZA
    {
        AUTOBUS, AVION, AUTOBUS_AVION, INDIVIDUALAN, OSTALO
    }

    
    public class Aranzman
    {
        public Aranzman(string naziv, TIP_ARANZMANA tipAranzmana, TIP_PREVOZA tipPrevoza, String lokacijaPutovanja, DateTime datumPocetka,
            DateTime datumZavrsetka, MestoNalazenja mesto, DateTime vremeNalazenja, uint maksPutnici, string opisAranzamana,
            string programPutovanja, string putanjaPostera, Guid smestajAr, bool obrisan)
        {
            Naziv = naziv; 
            TipAranzmana = tipAranzmana; 
            TipPrevoza = tipPrevoza; 
            LokacijaPutovanja = lokacijaPutovanja; 
            DatumPocetka = datumPocetka;
            DatumZavrsetka = datumZavrsetka;
            Mesto = mesto;
            VremeNalazenja = vremeNalazenja; 
            MaksPutnici = maksPutnici;
            OpisAranzamana = opisAranzamana;
            ProgramPutovanja = programPutovanja;
            PutanjaPostera = putanjaPostera;
            SmestajAr = smestajAr;
            Obrisan = obrisan;
            MyGuid = Guid.NewGuid();
        }

        public Aranzman()
        {
            Naziv = String.Empty;
            TipAranzmana = TIP_ARANZMANA.NAJAM_APARTMANA;
            TipPrevoza = TIP_PREVOZA.OSTALO;
            LokacijaPutovanja = String.Empty;
            DatumPocetka = DateTime.Now;
            DatumZavrsetka = DateTime.Now;
            Mesto = new MestoNalazenja();
            VremeNalazenja = DateTime.Now;
            MaksPutnici = 0;
            OpisAranzamana = String.Empty;
            ProgramPutovanja = String.Empty;
            SmestajAr = Guid.Empty;
            Obrisan = false;
            MyGuid = Guid.NewGuid();
        }

        public Aranzman(Aranzman ref1)
        {
            Naziv = ref1.Naziv;
            TipAranzmana = ref1.TipAranzmana;
            TipPrevoza = ref1.TipPrevoza;
            LokacijaPutovanja = ref1.LokacijaPutovanja;
            DatumPocetka = ref1.DatumPocetka;
            DatumZavrsetka = ref1.DatumZavrsetka;
            Mesto = ref1.Mesto;
            VremeNalazenja = ref1.VremeNalazenja;
            MaksPutnici = ref1.MaksPutnici;
            OpisAranzamana = ref1.OpisAranzamana;
            ProgramPutovanja = ref1.ProgramPutovanja;
            SmestajAr = ref1.SmestajAr;
            Obrisan = ref1.Obrisan;
            MyGuid = ref1.MyGuid;
        }

        public String Naziv { get; set; }
        public TIP_ARANZMANA TipAranzmana { get; set; }
        public TIP_PREVOZA TipPrevoza { get; set; }
        public String LokacijaPutovanja { get; set; }
        public DateTime DatumPocetka { get; set; }
        public DateTime DatumZavrsetka { get; set; }
        public MestoNalazenja Mesto { get; set; }
        public DateTime VremeNalazenja { get; set; }
        public uint MaksPutnici { get; set; }
        public String OpisAranzamana { get; set; }
        public String ProgramPutovanja { get; set; }
        public String PutanjaPostera { get; set; } 
        public Guid SmestajAr { get; set; }
        public bool Obrisan { get; set; }
        public Guid MyGuid { get; set; }
        public override string ToString()
        {
            String retVal = String.Empty;
            retVal += Naziv + "\n";
            retVal += Enum.GetName(typeof(TIP_ARANZMANA), TipAranzmana) + "\n";
            retVal += Enum.GetName(typeof(TIP_PREVOZA), TipPrevoza) + "\n";
            retVal += LokacijaPutovanja + "\n";
            retVal += DatumPocetka.ToString("dd/MM/yyyy") + "\n";
            retVal += DatumZavrsetka.ToString("dd/MM/yyyy") + "\n";
            retVal += Mesto.ToString() + "\n";
            retVal += VremeNalazenja.ToShortTimeString() + "\n";
            retVal += MaksPutnici.ToString() + "\n";
            retVal += OpisAranzamana + "\n";
            retVal += ProgramPutovanja + "\n";
            retVal += PutanjaPostera + "\n";
            retVal += SmestajAr.ToString() + "\n";
            retVal += Obrisan ? "Obrisan" : "Nije obrisan" + "\n";

            return retVal;
        }

        public override bool Equals(object obj)
        {
            try
            {
                return
                    MyGuid.Equals(((Aranzman)obj).MyGuid);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public String GetHashString()
        {
            var sha = SHA256.Create();

            var bytes = Encoding.ASCII.GetBytes(this.ToString());

            var hash = sha.ComputeHash(bytes);

            return BitConverter.ToString(hash, 0, 15);
        }
    }
}