﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR034_2018_WebProjekat.Models
{
    public class Administrator : Korisnik
    {
        public Administrator():base()
        {
            base.UlogaKorisnika = ULOGA_KORISNIKA.ADMINISTRATOR;
        }

        public Administrator(string korisnickoIme, string lozinka, string ime, string prezime, char pol, string email, DateTime datumRodjenja)
            : base(korisnickoIme, lozinka, ime, prezime, pol, email, datumRodjenja, ULOGA_KORISNIKA.ADMINISTRATOR)
        {
            
        }

        public Administrator(Administrator ref1)
        {
            KorisnickoIme = ref1.KorisnickoIme;
            Lozinka = ref1.Lozinka;
            Ime = ref1.Ime;
            Prezime = ref1.Prezime;
            Email = ref1.Email;
            DatumRodjenja = ref1.DatumRodjenja;
            UlogaKorisnika = ref1.UlogaKorisnika;
        }
    }
}