﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR034_2018_WebProjekat.Models
{
    public class MestoNalazenja
    {
        public MestoNalazenja(String adresaNalazenja, double geografskaDuzina, double geografskaSirina)
        {
            AdresaNalazenja = adresaNalazenja;
            GeografskaDuzina = geografskaDuzina;
            GeografskaSirina = geografskaSirina;
        }

        public MestoNalazenja()
        {
            AdresaNalazenja = String.Empty;
            GeografskaDuzina = 0;
            GeografskaSirina = 0;
        }

        public MestoNalazenja(MestoNalazenja ref1)
        {
            AdresaNalazenja = ref1.AdresaNalazenja;
            GeografskaDuzina = ref1.GeografskaDuzina;
            GeografskaSirina = ref1.GeografskaSirina;
        }

        public String AdresaNalazenja { get; set; }
        public Double GeografskaDuzina { get; set; }
        public Double GeografskaSirina { get; set; }

        public override bool Equals(object obj)
        {
            MestoNalazenja mestoNalazenja;

            try
            {
                mestoNalazenja = (MestoNalazenja)obj;
            }
            catch (Exception e)
            {
                return false;
            }

            return 
                mestoNalazenja.AdresaNalazenja.Equals(AdresaNalazenja) &&
                mestoNalazenja.GeografskaDuzina == GeografskaDuzina &&
                mestoNalazenja.GeografskaSirina == GeografskaSirina
                ;
        }
    }
}