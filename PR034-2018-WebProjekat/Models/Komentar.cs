﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace PR034_2018_WebProjekat.Models
{
    public class Komentar
    {
        public Komentar(Guid turistaKom, Guid aranzmanKom, string tekst, uint ocena, bool odobren, bool zabranjen)
        {
            TuristaKom = turistaKom;
            AranzmanKom = aranzmanKom;
            Tekst = tekst;
            Ocena = ocena;
            Odobren = odobren;
            Zabranjen = zabranjen;
            MyGuid = Guid.NewGuid();
        }

        public Komentar()
        {
            TuristaKom = Guid.NewGuid();
            AranzmanKom = Guid.NewGuid();
            Tekst = String.Empty;
            Ocena = 0;
            Odobren = false;
            Zabranjen = false;
            MyGuid = Guid.NewGuid();
        }

        public Komentar(Komentar ref1)
        {
            TuristaKom = ref1.TuristaKom;
            AranzmanKom = ref1.AranzmanKom;
            Tekst = ref1.Tekst;
            Ocena = ref1.Ocena;
            Odobren = ref1.Odobren;
            Zabranjen = ref1.Zabranjen;
            MyGuid = Guid.NewGuid();
        }

        public Guid TuristaKom { get; set; }
        public Guid AranzmanKom { get; set; }
        public String Tekst { get; set; }
        public uint Ocena { get; set; }
        public bool Odobren { get; set; }
        public Guid MyGuid { get; set; }
        public bool Zabranjen { get; set; }

        public override string ToString()
        {
            String retVal = String.Empty;   

            //retVal += TuristaKom.ToString() + "\n";
            //retVal += AranzmanKom.ToString() + "\n";
            //retVal += Tekst + "\n";
            //retVal += Ocena + "\n";
            //retVal += Odobren ? "Odobren" + "\n" : "Nije odobren" + "\n";

            return retVal;
        }

        public String GetHashString()
        {
            var sha = SHA256.Create();

            var bytes = Encoding.ASCII.GetBytes(this.ToString() + DateTime.Now.ToString());

            var hash = sha.ComputeHash(bytes);

            return BitConverter.ToString(hash, 0, 15);
        }

        public override bool Equals(object obj)
        {
            try
            {
                return
                    this.MyGuid.Equals(((Komentar)obj).MyGuid);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}