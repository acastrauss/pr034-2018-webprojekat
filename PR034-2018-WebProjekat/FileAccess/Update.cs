﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using PR034_2018_WebProjekat.Models;

namespace PR034_2018_WebProjekat.FileAccess
{
    public class Update
    {
        static String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");

        public static void UpdateAranzman(Aranzman newA)
        {
            String filePath = Path.Combine(wd, "Aranzmani\\", newA.MyGuid.ToString() + ".json");
            
            if(File.Exists(filePath))
            {
                File.WriteAllText(filePath, String.Empty);
                File.WriteAllText(filePath, JsonConvert.SerializeObject(newA, Formatting.Indented));
            }
        }

        public static void UpdateSmestaj(Smestaj newS)
        {
            String filePath = Path.Combine(wd, "Smestajevi\\",
                (newS.TipSmestaja == TIP_SMESTAJA.HOTEL ? "Hoteli\\" : "Ostali\\"),
                newS.MyGuid.ToString() + ".json");

            if (File.Exists(filePath))
            {
                File.WriteAllText(filePath, String.Empty);
                File.WriteAllText(filePath, JsonConvert.SerializeObject(newS, Formatting.Indented));
            }
        }

        public static void UpdateSmestajnaJedinica(SmestajnaJedinica newSj)
        {
            String filePath = Path.Combine(wd, "SmestajneJedinice\\",
                newSj.MyGuid.ToString() + ".json");

            if (File.Exists(filePath))
            {
                File.WriteAllText(filePath, String.Empty);
                File.WriteAllText(filePath, JsonConvert.SerializeObject(newSj, Formatting.Indented));
            }
        }
    
        public static void UpdateRezervacija(Rezervacija newRezervacija)
        {
            String filePath = Path.Combine(wd, "Rezervacije\\",
                newRezervacija.MyGuid.ToString() + ".json");

            if (File.Exists(filePath))
            {
                File.WriteAllText(filePath, String.Empty);
                File.WriteAllText(filePath, JsonConvert.SerializeObject(newRezervacija, Formatting.Indented));
            }
        }

        public static void UpdateKomentar(Komentar newKommentar)
        {
            String filePath = Path.Combine(wd, "Komentari\\",
                newKommentar.MyGuid.ToString() + ".json");

            if (File.Exists(filePath))
            {
                File.WriteAllText(filePath, String.Empty);
                File.WriteAllText(filePath, JsonConvert.SerializeObject(newKommentar, Formatting.Indented));
            }
        }

        public static void UpdateKorisnik<T>(T newUser) 
        {
            //TEST!
            String pathExt = String.Empty;
            object obj;
            var k = newUser as Korisnik;
            switch (k.UlogaKorisnika)
            {
                case ULOGA_KORISNIKA.ADMINISTRATOR:
                    obj = newUser as Administrator;
                    pathExt = "Administratori\\";
                    break;
                case ULOGA_KORISNIKA.MENADZER:
                    obj = newUser as Menadzer;
                    pathExt = "Menadzeri\\";
                    break;
                case ULOGA_KORISNIKA.TURISTA:
                    obj = newUser as Turista;
                    pathExt = "Turisti\\";
                    break;
                default:
                    return;
            }

            String filePath = Path.Combine(wd, "Korisnici\\", pathExt, k.MyGuid.ToString() + ".json");

            if(File.Exists(filePath)) 
            {
                File.WriteAllText(filePath, String.Empty);
                File.WriteAllText(filePath, JsonConvert.SerializeObject(obj, Formatting.Indented));
            }
        }
    }
}