﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using PR034_2018_WebProjekat.Models;

namespace PR034_2018_WebProjekat.FileAccess
{
    public class Delete
    {

        static String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");

        public static void DeleteAranzman(String arGUID) 
        {
            String file = Path.Combine(wd, "Aranzmani\\", arGUID + ".json");

            if(File.Exists(file))
            {
                var aranzman = JsonConvert.DeserializeObject<Aranzman>(File.ReadAllText(file));
                aranzman.Obrisan = true;
                File.WriteAllText(file, String.Empty);
                File.WriteAllText(file, JsonConvert.SerializeObject(aranzman, Formatting.Indented));
            }
        }

        public static void DeleteSmestaj(String smestajGUID) 
        {
            String hotelFile = Path.Combine(wd, "Smestajevi\\Hoteli\\", smestajGUID + ".json");
            String ostaliFile = Path.Combine(wd, "Smestajevi\\Ostali\\", smestajGUID + ".json");

            if(File.Exists(hotelFile))
            {
                var hotel = JsonConvert.DeserializeObject<Hotel>(File.ReadAllText(hotelFile));
                hotel.Obrisan = true;
                File.WriteAllText(hotelFile, String.Empty);
                File.WriteAllText(hotelFile,
                    JsonConvert.SerializeObject(hotel, Formatting.Indented));
            }
            else if (File.Exists(ostaliFile)) 
            {
                var smestaj = JsonConvert.DeserializeObject<Smestaj>(File.ReadAllText(ostaliFile));
                smestaj.Obrisan = true;
                File.WriteAllText(ostaliFile, String.Empty);
                File.WriteAllText(ostaliFile,
                    JsonConvert.SerializeObject(smestaj, Formatting.Indented));

            }
        }

    }
}