﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using PR034_2018_WebProjekat.Models;

namespace PR034_2018_WebProjekat.FileAccess
{
    public class Create
    {
        static String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");

        public static void AddAranzman(Aranzman aranzman)
        {
            String json = JsonConvert.SerializeObject(aranzman, Formatting.Indented);

            String fileName = String.Format("{0}.json", aranzman.MyGuid.ToString());

            String arFile = Path.Combine(wd, "Aranzmani\\", fileName);

            File.WriteAllText(arFile, json);
        }
        
        public static void AddMenadzer(Menadzer menadzer)
        {
            String json = JsonConvert.SerializeObject(menadzer, Formatting.Indented);

            String fileName = String.Format("{0}.json", menadzer.MyGuid.ToString());

            String managFile = Path.Combine(wd, "Korisnici\\Menadzeri\\", fileName);

            File.WriteAllText(managFile, json);
        }
        
        public static void AddAdmin(Administrator administrator)
        {
            String json = JsonConvert.SerializeObject(administrator, Formatting.Indented);

            String fileName = String.Format("{0}.json", administrator.MyGuid.ToString());

            String managFile = Path.Combine(wd, "Korisnici\\Administratori\\", fileName);

            File.WriteAllText(managFile, json);
        }

        public static void AddTurista(Turista turista)
        {
            String json = JsonConvert.SerializeObject(turista, Formatting.Indented);

            String fileName = String.Format("{0}.json", turista.MyGuid.ToString());

            String turFile = Path.Combine(wd, "Korisnici\\Turisti\\", fileName);

            File.WriteAllText(turFile, json);
        }

        public static void AddHotel(Hotel hotel)
        {
            String json = JsonConvert.SerializeObject(hotel, Formatting.Indented);

            String fileName = String.Format("{0}.json", hotel.MyGuid.ToString());

            String hotelFile = Path.Combine(wd, "Smestajevi\\Hoteli\\", fileName);

            File.WriteAllText(hotelFile, json);
        }

        public static void AddOstaliSmestaj(Smestaj smestaj)
        {
            String json = JsonConvert.SerializeObject(smestaj, Formatting.Indented);

            String fileName = String.Format("{0}.json", smestaj.MyGuid.ToString());

            String ostaliFile = Path.Combine(wd, "Smestajevi\\Ostali\\", fileName);

            File.WriteAllText(ostaliFile, json);
        }

        public static void AddRezervacija(Rezervacija rezervacija)
        {
            String json = JsonConvert.SerializeObject(rezervacija, Formatting.Indented);

            String fileName = String.Format("{0}.json", rezervacija.MyGuid.ToString());

            String rezFile = Path.Combine(wd, "Rezervacije\\", fileName);
            
            File.WriteAllText(rezFile, json);
        }

        public static void AddKomentar(Komentar komentar)
        {
            String json = JsonConvert.SerializeObject(komentar, Formatting.Indented);

            String fileName = String.Format("{0}.json", komentar.MyGuid.ToString());

            String komFile = Path.Combine(wd, "Komentari\\", fileName);
            
            File.WriteAllText(komFile, json);
        }

        public static void AddSmestajnaJedinica(SmestajnaJedinica smestajnaJedinica) 
        {
            String json = JsonConvert.SerializeObject(smestajnaJedinica, Formatting.Indented);

            String fileName = String.Format("{0}.json", smestajnaJedinica.MyGuid.ToString());

            String sjFile = Path.Combine(wd, "SmestajneJedinice\\", fileName);

            File.WriteAllText(sjFile, json);
        }
    }
}