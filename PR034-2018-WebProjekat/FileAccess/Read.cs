﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using PR034_2018_WebProjekat.Models;


namespace PR034_2018_WebProjekat.FileAccess
{
    public class Read
    {
        static String wd = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");

        #region Smestaj i Smestajne Jedinice
        public static List<SmestajnaJedinica> ReadAllSmestajneJedinice() =>
            Directory.GetFiles(Path.Combine(wd, "SmestajneJedinice\\")).ToList().
            Select(x => JsonConvert.DeserializeObject<SmestajnaJedinica>
            (File.ReadAllText(x))).
            ToList();

        public static SmestajnaJedinica ReadSmestajnaJedinica(String sjGUID) =>
            File.Exists(Path.Combine(wd, "SmestajneJedinice\\", sjGUID + ".json")) ?
            JsonConvert.DeserializeObject<SmestajnaJedinica>
            (File.ReadAllText(Path.Combine(wd, "SmestajneJedinice\\", sjGUID + ".json"))) :
            null;

        public static List<Smestaj> ReadAllOstaliSmestajevi() =>
            Directory.GetFiles(Path.Combine(wd, "Smestajevi\\Ostali\\")).ToList().
            Select(x => JsonConvert.DeserializeObject<Smestaj>
            (File.ReadAllText(x))).
            ToList();

        public static List<Hotel> ReadAllHoteli() =>
            Directory.GetFiles(Path.Combine(wd, "Smestajevi\\Hoteli\\")).ToList().
            Select(x => JsonConvert.DeserializeObject<Hotel>
            (File.ReadAllText(x))).
            ToList();

        public static Smestaj ReadSmestaj(String smestajGUID) => 
            File.Exists(Path.Combine(wd, "Smestajevi\\Ostali\\", smestajGUID + ".json")) ?
            JsonConvert.DeserializeObject<Smestaj>
            (File.ReadAllText(Path.Combine(wd, "Smestajevi\\Ostali\\", smestajGUID + ".json"))) :
            null;

        public static Hotel ReadHotel(String hotelGUID) =>
            File.Exists(Path.Combine(wd, "Smestajevi\\Hoteli\\", hotelGUID + ".json")) ?
            JsonConvert.DeserializeObject<Hotel>
            (File.ReadAllText(Path.Combine(wd, "Smestajevi\\Hoteli\\", hotelGUID + ".json"))) :
            null;
        #endregion

        #region Aranzamani

        public static List<Aranzman> ReadAllAranzmane() =>
            Directory.GetFiles(Path.Combine(wd, "Aranzmani\\")).ToList().
            Select(x => JsonConvert.DeserializeObject<Aranzman>
            (File.ReadAllText(x))).ToList();

        public static Aranzman ReadAranzman(String arGUID) =>
            File.Exists(Path.Combine(wd, "Aranzmani\\", arGUID + ".json")) ?
            JsonConvert.DeserializeObject<Aranzman>
            (File.ReadAllText(Path.Combine(wd, "Aranzmani\\", arGUID + ".json"))) :
            null;

        #endregion

        #region Administratori
        public static List<Administrator> ReadAllAdmins() =>
            Directory.GetFiles(Path.Combine(wd, "Korisnici\\Administratori\\")).ToList().
            Select(x => JsonConvert.DeserializeObject<Administrator>
            (File.ReadAllText(x))).ToList();

        public static Administrator FindAdmin(String adminGUID) =>
            File.Exists(Path.Combine(wd, "Korisnici\\Administratori\\", adminGUID + ".json")) ?
            JsonConvert.DeserializeObject<Administrator>
            (File.ReadAllText(Path.Combine(wd, "Korisnici\\Administratori\\", adminGUID + ".json"))) :
            null;

        #endregion

        #region Menadzeri
        public static List<Menadzer> ReadAllManagers() =>
            Directory.GetFiles(Path.Combine(wd, "Korisnici\\Menadzeri\\")).ToList().
            Select(x => JsonConvert.DeserializeObject<Menadzer>
            (File.ReadAllText(x))).ToList();

        public static Menadzer FindManager(String managGUID) =>
            File.Exists(Path.Combine(wd, "Korisnici\\Menadzeri\\", managGUID + ".json")) ?
            JsonConvert.DeserializeObject<Menadzer>
            (File.ReadAllText(Path.Combine(wd, "Korisnici\\Menadzeri\\", managGUID + ".json"))) :
            null;

        #endregion

        #region Turisti
        public static List<Turista> ReadAllToursits() =>
            Directory.GetFiles(Path.Combine(wd, "Korisnici\\Turisti\\")).ToList().
            Select(x => JsonConvert.DeserializeObject<Turista>
            (File.ReadAllText(x))).ToList();

        public static Turista FindTurista(String turGUID) =>
            File.Exists(Path.Combine(wd, "Korisnici\\Turisti\\", turGUID + ".json")) ?
            JsonConvert.DeserializeObject<Turista>
            (File.ReadAllText(Path.Combine(wd, "Korisnici\\Turisti\\", turGUID + ".json"))) :
            null;

        #endregion


        #region Rezervacije

        public static List<Rezervacija> ReadAllReservations() =>
            Directory.GetFiles(Path.Combine(wd, "Rezervacije\\")).ToList().
            Select(x => JsonConvert.DeserializeObject<Rezervacija>
            (File.ReadAllText(x))).ToList();

        public static Rezervacija FindRezervacija(String rezGUID) =>
            File.Exists(Path.Combine(wd, "Rezervacije\\", rezGUID + ".json")) ?
            JsonConvert.DeserializeObject<Rezervacija>
            (File.ReadAllText(Path.Combine(wd, "Rezervacije\\", rezGUID + ".json"))) :
            null;

        #endregion

        #region Komentari
        public static List<Komentar> ReadAllComments() =>
            Directory.GetFiles(Path.Combine(wd, "Komentari\\")).ToList().
            Select(x => JsonConvert.DeserializeObject<Komentar>
            (File.ReadAllText(x))).ToList();

        public static Komentar FindKomentar(String komGUID) =>
            File.Exists(Path.Combine(wd, "Komentari\\", komGUID + ".json")) ?
            JsonConvert.DeserializeObject<Komentar>
            (File.ReadAllText(Path.Combine(wd, "Komentari\\", komGUID + ".json"))) :
            null;

        #endregion

        #region Ptt

        public static Int32 GetPostanskiBroj(String grad) 
        {
            Int32 retVal = 0;

            String pttDir = wd + "Ptt\\";

            pttDir = Path.Combine(pttDir, "pttMesta.json");

            try
            {
                var json = File.ReadAllText(pttDir);
                var deserJson = JsonConvert.DeserializeObject<Dictionary<Int32, String>>(File.ReadAllText(json));

                retVal = deserJson.Where(x => x.Value.Equals(grad)).ToList()
                    .OrderBy(x => x.Key).FirstOrDefault().Key;
            }
            catch (Exception)
            {

            }
            
            return retVal;
        }

        #endregion
    }
}