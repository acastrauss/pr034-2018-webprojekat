﻿using System.Web;
using System.Web.Mvc;

namespace PR034_2018_WebProjekat
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
