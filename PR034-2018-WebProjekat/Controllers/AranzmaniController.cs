﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PR034_2018_WebProjekat.Models;
using PR034_2018_WebProjekat.FileAccess;
using Newtonsoft.Json;

namespace PR034_2018_WebProjekat.Controllers
{
    public class AranzmaniController : ApiController
    {
        // GET api/<controller>
        [Route("api/Aranzmani/GetAll")]
        public IHttpActionResult GetAll()
        {
            var ars = Read.ReadAllAranzmane();

            List<String> json = new List<String>();

            foreach (var a in ars)
            {
                json.Add(JsonConvert.SerializeObject(a));
            }
            
            var jsonArr = new[] { json };
            return Json(jsonArr);
        }

        // GET api/<controller>/5
        [Route("api/Aranzmani/GetMultiple")]
        public IHttpActionResult GetMultiple([FromUri] string[] guids)
        {
            return
                Ok(
                guids.ToList().Select(x =>
                Read.ReadAranzman(x)));
        }
        
        [Route("api/Aranzmani/GetOne")]
        public Aranzman GetOne([FromUri] string guid)
        {
            return
                Read.ReadAranzman(guid);
        }

        // POST api/<controller>
        public IHttpActionResult Post([FromBody] object value)
        {
            try
            {
                // send guid as cookie
                var cookie = Request.Headers.GetCookies().FirstOrDefault();
                
                var userName = Request.Headers.GetValues("username").FirstOrDefault();
                
                var aranzman = JsonConvert.DeserializeObject<Aranzman>(value.ToString());
                
                Create.AddAranzman(aranzman);
                
                Menadzer newM = Read.FindManager(cookie.Cookies[0].Name);
                newM.Aranzmani.Add(aranzman.MyGuid);

                Update.UpdateKorisnik(newM);

                return Json(new { naziv = aranzman.Naziv }) ;
            }
            catch (Exception e)
            {
                return InternalServerError(e.InnerException);
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        [Route("api/Aranzmani/DeleteGuid")]
        public IHttpActionResult DeleteGuid([FromUri] String guid)
        {
            try
            {
                FileAccess.Delete.DeleteAranzman(guid);
                return Ok("Deleted");
            }
            catch (Exception e)
            {
                return InternalServerError(e.InnerException);
            }
        }
    }
}