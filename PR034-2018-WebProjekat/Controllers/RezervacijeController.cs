﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PR034_2018_WebProjekat.Models;
using PR034_2018_WebProjekat.FileAccess;
using Newtonsoft.Json;

namespace PR034_2018_WebProjekat.Controllers
{
    public class RezervacijeController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }


        [HttpGet]
        [Route("api/Rezervacije/GetReservationsForManager")]
        public IHttpActionResult GetReservationsForManager([FromUri] string guid)
        {
            try
            {
                var manager = Read.FindManager(guid);
                var allRes = Read.ReadAllReservations();
                var retVal = new List<Rezervacija>();

                foreach (var r in allRes)
                {
                    if (manager.Aranzmani.Select(x => x.Equals(r.MyGuid)).Count() != 0)
                        retVal.Add(r);
                }

                return Ok(retVal);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("api/Rezervacija/GetMultiple")]
        // GET api/<controller>/5
        public IEnumerable<Rezervacija> GetMultiple([FromUri] string[] guids)
        {
            return guids.ToList().Select(x => Read.FindRezervacija(x));
        }
        

        [HttpPost]
        [Route("api/Rezervacije/Cancel")]
        public IHttpActionResult Cancel([FromBody] object guid)
        {
            var guids = JsonConvert.DeserializeObject<Dictionary<string, string>>(guid.ToString());
            var resGuid = guids["Reservation"];
            var sjGuid = guids["Sj"];

            if (!String.IsNullOrEmpty(resGuid) && !String.IsNullOrEmpty(sjGuid))
            {
                
                try
                {
                    var res = Read.FindRezervacija(resGuid);
                    if (res == null) return NotFound();
                    res.StatusRezervacije = STATUS_REZERVACIJE.OTKAZANA;
                    Update.UpdateRezervacija(res);

                    var sj = Read.ReadSmestajnaJedinica(sjGuid);
                    if (sj == null) return NotFound();
                    sj.Rezervisana = false;
                    Update.UpdateSmestajnaJedinica(sj);

                }
                catch (Exception)
                {
                    return InternalServerError();
                }

                return Ok(resGuid);
            }
            else
            {
                return NotFound();
            }
        }

        // POST api/<controller>
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}