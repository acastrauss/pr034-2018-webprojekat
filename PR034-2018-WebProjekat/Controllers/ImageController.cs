﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace PR034_2018_WebProjekat.Controllers
{
    public class ImageController : ApiController
    { 
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost]
        // POST api/<controller>
        public async Task<IHttpActionResult> Post()
        {
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);

            var fileNameParam = provider.Contents[0].Headers.ContentDisposition.Parameters
           .FirstOrDefault(p => p.Name.ToLower() == "filename");
            
            string fileName = (fileNameParam == null) ? "" : fileNameParam.Value.Trim('"');
            
            byte[] file = await provider.Contents[0].ReadAsByteArrayAsync();

            var rootPath = HttpContext.Current.Server.MapPath("~/Images/");

            try
            {
                if (String.IsNullOrEmpty(fileName))
                    fileName = Guid.NewGuid().ToString();

                var fname = Path.Combine(rootPath, fileName);

                var fstream = File.Create(fname);

                fstream.Write(file, 0, file.Length);

                fstream.Close();
                
                return Json(new { path = "\\Images\\" + fileName });
            }
            catch (Exception e)
            {
                return InternalServerError(e.InnerException);
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}