﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PR034_2018_WebProjekat.Models;
using PR034_2018_WebProjekat.FileAccess;

namespace PR034_2018_WebProjekat.Controllers
{
    public class KomentarController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet]
        [Route("api/Komentar/GetAll")]
        // GET api/<controller>/5
        public IEnumerable<Komentar> GetAll()
        {
            return 
                Read.ReadAllComments();
        }

        [HttpGet]
        [Route("api/Komentar/GetCommentsForManager")]
        // GET api/<controller>/5
        public IHttpActionResult GetCommentsForManager([FromUri] string guid)
        {
            var retVal = new List<Komentar>();

            try
            {
                var allComments = Read.ReadAllComments();
                var manag = Read.FindManager(guid);

                foreach (var c in allComments)
                {
                    if(manag.Aranzmani.Contains(c.AranzmanKom))
                    {
                        retVal.Add(c);
                    }
                }

                return Ok(retVal);
            }
            catch (Exception)
            {
                return InternalServerError();
            }

        }

        /// <summary>
        /// Number of unapproved comments
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Komentar/GetCommentsNumberForManager")]
        // GET api/<controller>/5
        public IHttpActionResult GetCommentsNumberForManager([FromUri] string guid)
        {
            var retVal = 0;

            try
            {
                var allComments = Read.ReadAllComments();
                var manag = Read.FindManager(guid);

                foreach (var c in allComments)
                {
                    if(manag.Aranzmani.Contains(c.AranzmanKom) && !c.Odobren && !c.Zabranjen)
                    {
                        retVal++;
                    }
                }

                return Ok(retVal);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("api/Komentar/ApproveBanComment")]
        public IHttpActionResult ApproveBanComment([FromBody] object json)
        {
            try
            {
                var jsonVal = JsonConvert.DeserializeObject<Dictionary<string, string>>(json.ToString());
                var komGuid = jsonVal["guid"];
                bool odobren = jsonVal["odobren"].Equals("true") ? true : false;
                bool zabranjen = jsonVal["zabranjen"].Equals("true") ? true : false;

                var k = Read.FindKomentar(komGuid);
                k.Odobren = odobren;
                k.Zabranjen = zabranjen;
                Update.UpdateKomentar(k);

                return Ok(k.MyGuid);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // POST api/<controller>
        public IHttpActionResult Post([FromBody] object value)
        {
            var k = JsonConvert.DeserializeObject<Komentar>(value.ToString());

            if(k != null)
            {
                Create.AddKomentar(k);
                // add guid to reservation
                return Ok(k.MyGuid);
            }
            else
            {
                return InternalServerError();
            }

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}