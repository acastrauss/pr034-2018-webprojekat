﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PR034_2018_WebProjekat.Models;
using PR034_2018_WebProjekat.FileAccess;
using Newtonsoft.Json;

namespace PR034_2018_WebProjekat.Controllers
{
    public class SmestajneJediniceController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<SmestajnaJedinica> Get()
        {
            return FileAccess.Read.ReadAllSmestajneJedinice();
        }

        [HttpGet]
        [Route("api/SmestajneJedinice/GetSmestajneJediniceForMultipleSmestaj")]
        public IHttpActionResult GetSmestajneJediniceForMultipleSmestaj([FromUri] string[] smestajGuids)
        {
            try
            {
                // key = smestaj.MyGuid
                var retVal = new Dictionary<string, List<SmestajnaJedinica>>();

                for (int i = 0; i < smestajGuids.Length; i++)
                {
                    var smestaj = Read.ReadSmestaj(smestajGuids[i]);
                    var hotel = Read.ReadHotel(smestajGuids[i]);

                    if (smestaj == null && hotel == null) continue;

                    var smestajUsing = smestaj != null ? smestaj : hotel;

                    retVal.Add(smestajUsing.MyGuid.ToString(), new List<SmestajnaJedinica>());

                    foreach (var sjGuid in smestajUsing.SmestajneJedinice)
                    {
                        var sj = Read.ReadSmestajnaJedinica(sjGuid.ToString());
                        retVal[smestajUsing.MyGuid.ToString()].Add(sj);
                    }

                }

                return Ok(JsonConvert.SerializeObject(retVal));   
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        //// GET api/<controller>/5
        //public SmestajnaJedinica Get([FromBody]String sjGUID)
        //{
        //    return FileAccess.Read.ReadSmestajnaJedinica(sjGUID);
        //}
        [Route("api/SmestajneJedinice/GetMultiple")]
        public IHttpActionResult GetMultiple([FromUri] String[] guids)
        {
            if(guids == null)
            {
                return Ok(new List<SmestajnaJedinica>());
            }
            else
            {
                if (guids.Length == 0) return Ok(new List<SmestajnaJedinica>());
                return Ok(
                   guids.ToList().
                   Select(x => Read.ReadSmestajnaJedinica(x)));
            }
        }
        
        [Route("api/SmestajneJedinice/GetOne")]
        public IHttpActionResult GetOne([FromUri] String guid)
        {
            return Ok(Read.ReadSmestajnaJedinica(guid));
        }

        [HttpPost]
        [Route("api/SmestajneJedinice/Reserve")]
        public IHttpActionResult Reserve([FromBody] object reservation)
        {
            var res = JsonConvert.DeserializeObject<Rezervacija>(reservation.ToString());

            var sj = Read.ReadSmestajnaJedinica(res.SmestajnaJedinicaRez.ToString());
            var t = Read.FindTurista(res.TuristaRez.ToString());
            var ar = Read.ReadAranzman(res.AranzmanRez.ToString());

            if (sj != null && t != null && ar != null)
            {
                Create.AddRezervacija(res);

                sj.Rezervisana = true;
                Update.UpdateSmestajnaJedinica(sj);

                t.Rezervacije.Add(res.MyGuid);
                
                Update.UpdateKorisnik(t);

                return Ok(sj.MyGuid);
            }
            else
            {
                // should not reach
                return NotFound();
            }
        }

        // POST api/<controller>
        public IHttpActionResult Post([FromBody] object value)
        {
            try
            {   
                List<String> guids = new List<string>();

                JsonConvert.DeserializeObject<List<SmestajnaJedinica>>(value.ToString())
                    .ForEach(x => {
                        if (FileAccess.Read.ReadSmestajnaJedinica(x.MyGuid.ToString()) == null)
                            FileAccess.Create.AddSmestajnaJedinica(x);
                        else
                            FileAccess.Update.UpdateSmestajnaJedinica(x);
                        guids.Add(x.MyGuid.ToString());
                    });
                    
                return Ok(guids);
            }
            catch (Exception e)
            {
                var a = e.Message;
                return InternalServerError();
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}