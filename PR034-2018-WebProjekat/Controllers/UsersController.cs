﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using PR034_2018_WebProjekat.Models;
using PR034_2018_WebProjekat.FileAccess;

namespace PR034_2018_WebProjekat.Controllers
{
    public class UsersController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet]
        [Route("api/Users/GetAllUsers")]
        public IHttpActionResult GetAllUsers()
        {
            try
            {
                var retVal = new List<Korisnik>();

                var turisti = Read.ReadAllToursits();
                var admins = Read.ReadAllAdmins();
                var managers = Read.ReadAllManagers();

                retVal.AddRange(turisti);
                retVal.AddRange(admins);
                retVal.AddRange(managers);

                return Ok(retVal);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        [HttpGet]
        [Route("api/Users/GetNumberPotentialToBlockUsers")]
        public IHttpActionResult GetNumberPotentialToBlockUsers([FromUri] string guid)
        {
            try
            {
                var admin = Read.FindAdmin(guid);

                if (admin == null)
                    return BadRequest();

                int retVal = 0;

                var allUsers = Read.ReadAllToursits();

                foreach (var u in allUsers)
                {
                    if(u.BrojOtkazanih >= 2 && !u.Blokiran)
                    {
                        retVal++;
                    }
                }

                return Ok(retVal);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("api/Users/GetPotentialToBlockUsers")]
        public IHttpActionResult GetPotentialToBlockUsers([FromUri] string guid)
        {
            try
            {
                var admin = Read.FindAdmin(guid);

                if (admin == null)
                    return BadRequest();

                List<Turista> potential = new List<Turista>();

                var allUsers = Read.ReadAllToursits();

                foreach (var u in allUsers)
                {
                    if (u.BrojOtkazanih >= 2 && !u.Blokiran)
                    {
                        potential.Add(u);
                    }
                }

                return Ok(potential);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("api/Users/BlockUser")]
        public IHttpActionResult BlockUser([FromBody] object json)
        {
            try
            {
                var guid = JsonConvert.DeserializeObject<Dictionary<string, string>>(json.ToString())["guid"];

                var user = Read.FindTurista(guid);

                if (user == null)
                    return NotFound();

                user.Blokiran = true;

                Update.UpdateKorisnik(user);

                return Ok(guid);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }


        [HttpGet]
        [Route("api/Users/GetOneUser")]
        // GET api/<controller>/username
        public IHttpActionResult GetOneUser([FromUri] string guid)
        {
            try
            {
                var a = Read.FindAdmin(guid);
                var m = Read.FindManager(guid);
                var t = Read.FindTurista(guid);

                if (a != null) return Ok(a);
                if (m != null) return Ok(m);
                if (t != null) return Ok(t);

                return NotFound();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost]
        // POST api/<controller>
        public IHttpActionResult Post([FromBody] object value)
        {
            Korisnik t = JsonConvert.DeserializeObject<Korisnik>(value.ToString());

            var oldTurist = FileAccess.Read.ReadAllToursits()
                .Where(x => x.KorisnickoIme.Equals(t.KorisnickoIme) && x.Lozinka.Equals(t.Lozinka))
                .FirstOrDefault();

            var oldManag = FileAccess.Read.ReadAllManagers()
                .Where(x => x.KorisnickoIme.Equals(t.KorisnickoIme) && x.Lozinka.Equals(t.Lozinka))
                .FirstOrDefault();

            var oldAdmin = FileAccess.Read.ReadAllAdmins()
                .Where(x => x.KorisnickoIme.Equals(t.KorisnickoIme) && x.Lozinka.Equals(t.Lozinka))
                .FirstOrDefault();

            if (t.Email.Equals(String.Empty)) 
            {               
                if (oldTurist != null)
                {
                    if (!oldTurist.Blokiran)
                        return Ok(new { cookie = oldTurist.Cookie, username = oldTurist.KorisnickoIme, role = oldTurist.UlogaKorisnika });
                    else
                        return BadRequest("You are blocked!");
                }
                else if (oldManag != null)
                {
                    return Ok(new { cookie = oldManag.Cookie, username = oldManag.KorisnickoIme, role = oldManag.UlogaKorisnika });
                }
                else if (oldAdmin != null)
                {
                    return Ok(new { cookie = oldAdmin.Cookie, username = oldAdmin.KorisnickoIme, role = oldAdmin.UlogaKorisnika });
                }
                else
                {
                    return BadRequest("Wrong credentials.");
                }
            }
            else
            {
                if(oldTurist != null || oldManag != null || oldAdmin != null)
                    return BadRequest("User already exists.");

                if(t.UlogaKorisnika == ULOGA_KORISNIKA.TURISTA)
                {
                    Turista tur = new Turista()
                    {
                        Blokiran = false,
                        BrojOtkazanih = 0,
                        DatumRodjenja = t.DatumRodjenja,
                        Email = t.Email,
                        Ime = t.Ime,
                        KorisnickoIme = t.KorisnickoIme,
                        Lozinka = t.Lozinka,
                        Pol = t.Pol,
                        Prezime = t.Prezime,
                        Rezervacije = new List<Guid>(),
                        UlogaKorisnika = t.UlogaKorisnika
                    };

                    Create.AddTurista(tur);
                }
                else if(t.UlogaKorisnika == ULOGA_KORISNIKA.MENADZER)
                {
                    Menadzer newManager = new Menadzer()
                    {
                        Aranzmani = new List<Guid>(),
                        Cookie = t.Cookie,
                        DatumRodjenja = t.DatumRodjenja,
                        Email = t.Email,
                        Ime = t.Ime,
                        KorisnickoIme = t.KorisnickoIme,
                        Lozinka = t.Lozinka,
                        MyGuid = t.MyGuid,
                        Pol = t.Pol,
                        Prezime = t.Prezime,
                        UlogaKorisnika = t.UlogaKorisnika
                    };

                    Create.AddMenadzer(newManager);
                }


                return Ok(new { cookie = t.Cookie, username = t.KorisnickoIme, role = t.UlogaKorisnika });
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}