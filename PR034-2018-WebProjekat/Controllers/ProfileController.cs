﻿using Newtonsoft.Json;
using PR034_2018_WebProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PR034_2018_WebProjekat.Controllers
{
    public class ProfileController : ApiController
    {
        
        // GET api/<controller>
        public IHttpActionResult Get()
        {
            var newJson = new Newtonsoft.Json.Linq.JObject();
            var paramsJson = Request.RequestUri.TryReadQueryAsJson(out newJson);
            if (!paramsJson) return BadRequest();
            
            
            var jsonParam = JsonConvert.DeserializeObject<Dictionary<string, string>>(newJson.ToString());

            Turista t = FileAccess.Read.FindTurista(jsonParam["UserGUID"]);
            Menadzer m = FileAccess.Read.FindManager(jsonParam["UserGUID"]);
            Administrator a = FileAccess.Read.FindAdmin(jsonParam["UserGUID"]);
            
            if (t != null)
            {
                var json = JsonConvert.SerializeObject(t);
                return Ok(json);
            }
            else if (m != null)
            {
                var json = JsonConvert.SerializeObject(m);
                return Ok(json);
            }
            else if (a != null)
            {
                var json = JsonConvert.SerializeObject(a);
                return Ok(json);
            }
            else
            {
                return BadRequest("User doesn't exist.");
            }
        }

        [HttpPost]
        // POST api/<controller>/username
        public IHttpActionResult Post([FromBody] object value)
        {
            var cookie = Request.Headers.GetCookies().FirstOrDefault();
            
            try
            {
                var k = JsonConvert.DeserializeObject<Korisnik>(value.ToString());

                var t = FileAccess.Read.FindAdmin(k.Cookie);
                var m = FileAccess.Read.FindManager(k.Cookie);
                var a = FileAccess.Read.FindTurista(k.Cookie);

                switch (k.UlogaKorisnika)
                {
                    case ULOGA_KORISNIKA.ADMINISTRATOR:
                        var adm = JsonConvert.DeserializeObject<Administrator>(value.ToString());
                        adm.MyGuid = a.MyGuid;
                        FileAccess.Update.UpdateKorisnik(adm);
                        break;
                    case ULOGA_KORISNIKA.MENADZER:
                        var man = JsonConvert.DeserializeObject<Menadzer>(value.ToString());
                        man.MyGuid = m.MyGuid;
                        FileAccess.Update.UpdateKorisnik(man);
                        break;
                    case ULOGA_KORISNIKA.TURISTA:
                        var tur = JsonConvert.DeserializeObject<Turista>(value.ToString());
                        tur.MyGuid = t.MyGuid;
                        FileAccess.Update.UpdateKorisnik(tur);
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {
                return InternalServerError();
            }

            return Ok();
        }
        
        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}