﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PR034_2018_WebProjekat.Models;
using PR034_2018_WebProjekat.FileAccess;
using Newtonsoft.Json;

namespace PR034_2018_WebProjekat.Controllers
{
    public class SmestajController : ApiController
    {
        [HttpGet]
        [Route("api/Smestaj/GetFreeSJForMultipleSmestajevi")]
        public IHttpActionResult GetFreeSJForMultipleSmestajevi([FromUri] string[] smestajGuids) 
        {
            try
            {
                var retVal = new Dictionary<string, int>();

                for (int i = 0; i < smestajGuids.Length; i++)
                {
                    var smestaj = Read.ReadSmestaj(smestajGuids[i]);
                    var hotel = Read.ReadHotel(smestajGuids[i]);

                    if (smestaj == null && hotel == null) continue;

                    var sjGuids = smestaj != null ? smestaj.SmestajneJedinice : hotel.SmestajneJedinice;
    
                    int freeOne = 0;

                    foreach (var sjGuid in sjGuids)
                    {
                        var sj = Read.ReadSmestajnaJedinica(sjGuid.ToString());
                        if (!sj.Rezervisana && !sj.Obrisana) freeOne++;
                    }

                    retVal.Add(smestajGuids[i], freeOne);
                }

                return Ok(JsonConvert.SerializeObject(retVal));
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // GET api/<controller>
        public IHttpActionResult Get()
        {
            var smestajevi = Read.ReadAllOstaliSmestajevi();
            smestajevi.AddRange(Read.ReadAllHoteli());

            List<string> jsonList = new List<string>();

            smestajevi.ForEach(x => jsonList.Add(JsonConvert.SerializeObject(x)));

            return Json(jsonList);
        }

        // GET api/<controller>/5
        public IHttpActionResult Get([FromUri] String guid)
        {
            var smestaj = Read.ReadSmestaj(guid);
            var hotel = Read.ReadHotel(guid);

            if (smestaj != null)
                return Ok(JsonConvert.SerializeObject(smestaj));
            if (hotel != null)
                return Ok(hotel);

            return NotFound();
        }

        // POST api/<controller>
        public IHttpActionResult Post([FromBody] object value)
        {
            Smestaj smestaj = JsonConvert.DeserializeObject<Smestaj>(value.ToString());
            Guid guidUsed = Guid.Empty;
            if(smestaj.TipSmestaja == TIP_SMESTAJA.HOTEL) 
            {
                Hotel hotelNew = JsonConvert.DeserializeObject<Hotel>(value.ToString());
                Hotel hotelOld = FileAccess.Read.ReadHotel(smestaj.MyGuid.ToString());
                
                if (hotelOld != null)
                {
                    hotelNew.MyGuid = hotelOld.MyGuid;
                    Update.UpdateSmestaj(hotelNew);
                }
                else
                {
                    Create.AddHotel(hotelNew);
                }
                guidUsed = hotelNew.MyGuid;
            }
            else
            {
                var smestajOld = FileAccess.Read.ReadSmestaj(smestaj.MyGuid.ToString());

                if (smestajOld != null)
                {
                    smestaj.MyGuid = smestajOld.MyGuid;
                    Update.UpdateSmestaj(smestaj);
                }
                else
                {
                    Create.AddOstaliSmestaj(smestaj);
                }

                guidUsed = smestaj.MyGuid;
            }

            return Ok(guidUsed);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public IHttpActionResult Delete([FromBody] object value)
        {
            try
            {
                var vals = JsonConvert.DeserializeObject<Dictionary<string, string>>(value.ToString());

                FileAccess.Delete.DeleteSmestaj(vals["smestajGuid"]);
                
                return Ok("Deleted!");
            }
            catch (Exception e)
            {
                return InternalServerError(e.InnerException);
            }
        }
    }
}